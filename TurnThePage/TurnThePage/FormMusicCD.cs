﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static TurnThePage.MusicCD;

namespace TurnThePage
{
    public partial class FormMusicCD : Form
    {
        public ShoppingCart shoppingCart = new ShoppingCart();
        SqlConnection connect;
        SqlCommand command;
        SqlDataAdapter sda;
        bool Admin;

        public FormMusicCD()
        {
            InitializeComponent();
        }

        private void FormMusicCD_Load(object sender, EventArgs e)
        {
                fillDataGrid();

            if (Admin == false)
            {
                this.Size = new Size(750, 501);
            }
            else
            {
                //this.Size = new Size(1085, 501);
                btnExt.Visible = false;
                btnSelect.Visible = false;
            }
        }

        public FormMusicCD(ShoppingCart shopping,bool admin)
        {
            InitializeComponent();
            shoppingCart = shopping;
            Admin = admin;
        }

        List<MusicCD> getMusicCD()
        {
            List<MusicCD> musicCDList = new List<MusicCD>();

            connect = new SqlConnection("Server= localhost\\sqlexpress; Database= dbTurnThePage; Integrated Security=True;");
            command = new SqlCommand();
            sda = new SqlDataAdapter();
            command.CommandText = "SELECT *FROM MusicCD_info";
            command.CommandType = CommandType.Text;

            connect.Open();
            command.Connection = connect;
            SqlDataReader dr = command.ExecuteReader();

            while (dr.Read())
            {
                MusicCD temp = new MusicCD();

                temp.Name = dr[0].ToString();
                temp.ID1 = Convert.ToInt32(dr[1]);
                temp.Price = (float)Convert.ToDouble(dr[2]);
                temp.Singer = dr[3].ToString();
                temp.TypeOfMusic = (Music_type)Convert.ToInt32(dr[4]);

                musicCDList.Add(temp);

            }
            connect.Close();

            return musicCDList;
        }

        void fillDataGrid()
        {

            dgwMusicCD.Rows.Clear();
            dgwMusicCD.Refresh();

            int i = 0;

            List<MusicCD> musicCDList = new List<MusicCD>();
            musicCDList = getMusicCD();

            dgwMusicCD.ColumnCount = 5;
            dgwMusicCD.Columns[0].Name = "ID";
            dgwMusicCD.Columns[1].Name = "Name";
            dgwMusicCD.Columns[2].Name = "Price";
            dgwMusicCD.Columns[3].Name = "Singer";
            dgwMusicCD.Columns[4].Name = "Type";

            dgwMusicCD.BorderStyle = BorderStyle.None;
            dgwMusicCD.AlternatingRowsDefaultCellStyle.BackColor = Color.Tan;
            dgwMusicCD.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dgwMusicCD.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dgwMusicCD.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dgwMusicCD.BackgroundColor = Color.White;

            dgwMusicCD.EnableHeadersVisualStyles = false;
            dgwMusicCD.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dgwMusicCD.ColumnHeadersDefaultCellStyle.BackColor = Color.SaddleBrown;
            dgwMusicCD.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

            dgwMusicCD.Columns[0].Width = 20;
            dgwMusicCD.Columns[2].Width = 30;

            while (i < musicCDList.Count)
            {
                string[] row = new string[] {musicCDList[i].ID1.ToString(), musicCDList[i].Name, musicCDList[i].Price.ToString(),
                    musicCDList[i].Singer.ToString(), musicCDList[i].TypeOfMusic.ToString()};
                dgwMusicCD.Rows.Add(row);
                i++;
            }
        }


        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void FormMusicCD_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void panelTop_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnExt_Click(object sender, EventArgs e)
        {
                this.Close();
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            pnlInfo.Visible = false;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgwMusicCD.SelectedRows)
            {
                MusicCD temp = new MusicCD();


                //row.Cells[1].Value.ToString();
                temp.ID1 = Convert.ToInt32(row.Cells[0].Value);
                temp.Name = row.Cells[1].Value.ToString();
                temp.Price = Convert.ToSingle(row.Cells[2].Value);
                temp.Singer = row.Cells[3].Value.ToString();

                int type = 0;

                switch (row.Cells[4].Value)
                {
                    case "Rock":
                        type = 0;
                        break;
                    case "Country":
                        type = 1;
                        break;
                    case "Slow":
                        type = 2;
                        break;
                    case "Pop":
                        type = 3;
                        break;
                    case "RnB":
                        type = 4;
                        break;
                    default:
                        type = 0;
                        break;
                }

                temp.TypeOfMusic = (Music_type)type;

                using (FormBuy formBuy = new FormBuy(temp, shoppingCart))
                {
                    if (formBuy.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        shoppingCart = formBuy.shoppingCart;
                    }
                }
            }
        }

        private void dgwMusicCD_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if(Admin == false)
            {
                foreach (DataGridViewRow row in dgwMusicCD.SelectedRows)
                {
                    MusicCD temp = new MusicCD();


                    //row.Cells[1].Value.ToString();
                    temp.ID1 = Convert.ToInt32(row.Cells[0].Value);
                    temp.Name = row.Cells[1].Value.ToString();
                    temp.Price = Convert.ToSingle(row.Cells[2].Value);
                    temp.Singer = row.Cells[3].Value.ToString();

                    int type = 0;

                    switch (row.Cells[4].Value)
                    {
                        case "Rock":
                            type = 0;
                            break;
                        case "Country":
                            type = 1;
                            break;
                        case "Slow":
                            type = 2;
                            break;
                        case "Pop":
                            type = 3;
                            break;
                        case "RnB":
                            type = 4;
                            break;
                        default:
                            type = 0;
                            break;
                    }

                    temp.TypeOfMusic = (Music_type)type;

                    using (FormBuy formBuy = new FormBuy(temp, shoppingCart))
                    {
                        if (formBuy.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            shoppingCart = formBuy.shoppingCart;
                        }
                    }
                }
            }
            
        }

        void addBook()
        {
            int type = 0;
            connect = new SqlConnection("Server= localhost\\sqlexpress; Database= dbTurnThePage; Integrated Security=True;");
            command = new SqlCommand("INSERT into MusicCD_info(name,price,singer,type) values (@name,@price,@singer,@type)", connect);
            sda = new SqlDataAdapter();



            command.Parameters.AddWithValue("@name", txtUsername.Text);
            command.Parameters.AddWithValue("@price", txtPrice.Text);
            command.Parameters.AddWithValue("@singer", txtSinger.Text);

            switch (cbType.Text)
            {
                case "Rock":
                    type = 0;
                    break;
                case "Country":
                    type = 1;
                    break;
                case "Slow":
                    type = 2;
                    break;
                case "Pop":
                    type = 3;
                    break;
                case "RnB":
                    type = 4;
                    break;
            }
            command.Parameters.AddWithValue("@type", type);

            connect.Open();
            //command.Connection = connect;
            command.ExecuteNonQuery();
            connect.Close();
            clearTxt();
        }

        void clearTxt()
        {
            txtSinger.Text = "";
            txtUsername.Text = "";
            txtPrice.Text = "";
            cbType.Text = "";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            addBook();
            fillDataGrid();
            
        }

        private void btnExt2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgwMusicCD_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
