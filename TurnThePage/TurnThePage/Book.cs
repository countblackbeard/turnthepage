﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnThePage
{
    public class Book:Product
    {
        private int isbn;
        private string author;
        private string publisher;
        private int page;
        private string cover_page_picture;

        public int Isbn { get => isbn; set => isbn = value; }
        public string Author { get => author; set => author = value; }
        public string Publisher { get => publisher; set => publisher = value; }
        public int Page { get => page; set => page = value; }
        public string Cover_page_picture { get => cover_page_picture; set => cover_page_picture = value; }
    }
}
