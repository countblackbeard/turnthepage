﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnThePage
{
    public abstract class Product
    {
        private string name;
        private int ID;
        private float price;

        public string Name { get => name; set => name = value; }
        public int ID1 { get => ID; set => ID = value; }
        public float Price { get => price; set => price = value; }
        

        public void printProperties()
        {
            Console.WriteLine("Name: " + Name + " ID: " + ID1 + " Price: " + price);
        }
    }
}
