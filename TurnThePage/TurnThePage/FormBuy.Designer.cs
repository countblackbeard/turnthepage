﻿namespace TurnThePage
{
    partial class FormBuy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTop = new System.Windows.Forms.Panel();
            this.btnExit = new System.Windows.Forms.Button();
            this.lblTurnThePage = new System.Windows.Forms.Label();
            this.btnExt = new System.Windows.Forms.Button();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblMinus = new System.Windows.Forms.Label();
            this.lblPlus = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.lblTL = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.lblL = new System.Windows.Forms.Label();
            this.pnlPrice = new System.Windows.Forms.Panel();
            this.panelTop.SuspendLayout();
            this.pnlPrice.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.Tan;
            this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTop.Controls.Add(this.btnExit);
            this.panelTop.Controls.Add(this.lblTurnThePage);
            this.panelTop.Controls.Add(this.btnExt);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(347, 39);
            this.panelTop.TabIndex = 2;
            this.panelTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelTop_MouseMove);
            // 
            // btnExit
            // 
            this.btnExit.Cursor = System.Windows.Forms.Cursors.No;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExit.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnExit.Location = new System.Drawing.Point(294, 3);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(48, 31);
            this.btnExit.TabIndex = 21;
            this.btnExit.Text = "X";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // lblTurnThePage
            // 
            this.lblTurnThePage.AutoSize = true;
            this.lblTurnThePage.BackColor = System.Drawing.Color.Transparent;
            this.lblTurnThePage.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTurnThePage.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblTurnThePage.Location = new System.Drawing.Point(3, 2);
            this.lblTurnThePage.Name = "lblTurnThePage";
            this.lblTurnThePage.Size = new System.Drawing.Size(184, 28);
            this.lblTurnThePage.TabIndex = 22;
            this.lblTurnThePage.Text = "You buying now...";
            // 
            // btnExt
            // 
            this.btnExt.Cursor = System.Windows.Forms.Cursors.No;
            this.btnExt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnExt.Location = new System.Drawing.Point(610, 3);
            this.btnExt.Name = "btnExt";
            this.btnExt.Size = new System.Drawing.Size(48, 31);
            this.btnExt.TabIndex = 3;
            this.btnExt.Text = "X";
            this.btnExt.UseVisualStyleBackColor = true;
            this.btnExt.Click += new System.EventHandler(this.btnExt_Click);
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblPrice.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblPrice.Location = new System.Drawing.Point(6, 8);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(56, 21);
            this.lblPrice.TabIndex = 9;
            this.lblPrice.Text = "Price";
            this.lblPrice.Click += new System.EventHandler(this.lblPrice_Click);
            // 
            // lblMinus
            // 
            this.lblMinus.AutoSize = true;
            this.lblMinus.BackColor = System.Drawing.Color.Transparent;
            this.lblMinus.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMinus.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblMinus.Location = new System.Drawing.Point(66, 147);
            this.lblMinus.Name = "lblMinus";
            this.lblMinus.Size = new System.Drawing.Size(17, 21);
            this.lblMinus.TabIndex = 10;
            this.lblMinus.Text = "-";
            this.lblMinus.Click += new System.EventHandler(this.lblMinus_Click);
            // 
            // lblPlus
            // 
            this.lblPlus.AutoSize = true;
            this.lblPlus.BackColor = System.Drawing.Color.Transparent;
            this.lblPlus.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlus.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblPlus.Location = new System.Drawing.Point(114, 147);
            this.lblPlus.Name = "lblPlus";
            this.lblPlus.Size = new System.Drawing.Size(20, 21);
            this.lblPlus.TabIndex = 11;
            this.lblPlus.Text = "+";
            this.lblPlus.Click += new System.EventHandler(this.lblPlus_Click);
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(89, 147);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(19, 20);
            this.txtQuantity.TabIndex = 12;
            this.txtQuantity.Text = "1";
            // 
            // lblTL
            // 
            this.lblTL.AutoSize = true;
            this.lblTL.BackColor = System.Drawing.Color.Transparent;
            this.lblTL.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTL.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblTL.Location = new System.Drawing.Point(542, 155);
            this.lblTL.Name = "lblTL";
            this.lblTL.Size = new System.Drawing.Size(37, 21);
            this.lblTL.TabIndex = 13;
            this.lblTL.Text = "TL";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblName.Location = new System.Drawing.Point(85, 83);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(79, 21);
            this.lblName.TabIndex = 14;
            this.lblName.Text = "Buying ";
            this.lblName.Click += new System.EventHandler(this.lblName_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Tan;
            this.btnAdd.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(128, 205);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(95, 33);
            this.btnAdd.TabIndex = 20;
            this.btnAdd.Text = "Add Cart";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblL
            // 
            this.lblL.AutoSize = true;
            this.lblL.BackColor = System.Drawing.Color.Transparent;
            this.lblL.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblL.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblL.Location = new System.Drawing.Point(73, 8);
            this.lblL.Name = "lblL";
            this.lblL.Size = new System.Drawing.Size(22, 21);
            this.lblL.TabIndex = 21;
            this.lblL.Text = "$";
            // 
            // pnlPrice
            // 
            this.pnlPrice.BackColor = System.Drawing.Color.Tan;
            this.pnlPrice.Controls.Add(this.lblPrice);
            this.pnlPrice.Controls.Add(this.lblL);
            this.pnlPrice.Location = new System.Drawing.Point(161, 139);
            this.pnlPrice.Name = "pnlPrice";
            this.pnlPrice.Size = new System.Drawing.Size(103, 40);
            this.pnlPrice.TabIndex = 22;
            // 
            // FormBuy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TurnThePage.Properties.Resources.BGi;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(347, 296);
            this.Controls.Add(this.pnlPrice);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblTL);
            this.Controls.Add(this.txtQuantity);
            this.Controls.Add(this.lblPlus);
            this.Controls.Add(this.lblMinus);
            this.Controls.Add(this.panelTop);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(347, 296);
            this.MinimumSize = new System.Drawing.Size(347, 296);
            this.Name = "FormBuy";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormBuy";
            this.Load += new System.EventHandler(this.FormBuy_Load);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.pnlPrice.ResumeLayout(false);
            this.pnlPrice.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Button btnExt;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblMinus;
        private System.Windows.Forms.Label lblPlus;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.Label lblTL;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Label lblTurnThePage;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label lblL;
        private System.Windows.Forms.Panel pnlPrice;
    }
}