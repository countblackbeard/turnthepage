﻿namespace TurnThePage
{
    partial class FormBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panelTop = new System.Windows.Forms.Panel();
            this.btnExt2 = new System.Windows.Forms.Button();
            this.lblBookName = new System.Windows.Forms.Label();
            this.lblTurnThePage = new System.Windows.Forms.Label();
            this.btnExt = new System.Windows.Forms.Button();
            this.dgwBooks = new System.Windows.Forms.DataGridView();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.lblName = new System.Windows.Forms.Label();
            this.btnInfo = new System.Windows.Forms.Button();
            this.pbBook = new System.Windows.Forms.PictureBox();
            this.booksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnBook = new System.Windows.Forms.Button();
            this.lblisbn = new System.Windows.Forms.Label();
            this.txtisbn = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblNName = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.lblPage = new System.Windows.Forms.Label();
            this.txtPage = new System.Windows.Forms.TextBox();
            this.lblPublisher = new System.Windows.Forms.Label();
            this.txtPublisher = new System.Windows.Forms.TextBox();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.txtauthor = new System.Windows.Forms.TextBox();
            this.lblDolar = new System.Windows.Forms.Label();
            this.ofdPic = new System.Windows.Forms.OpenFileDialog();
            this.btnPhoto = new System.Windows.Forms.Button();
            this.lblPicName = new System.Windows.Forms.Label();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwBooks)).BeginInit();
            this.pnlInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.booksBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.Tan;
            this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTop.Controls.Add(this.btnExt2);
            this.panelTop.Controls.Add(this.lblBookName);
            this.panelTop.Controls.Add(this.lblTurnThePage);
            this.panelTop.Controls.Add(this.btnExt);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(982, 39);
            this.panelTop.TabIndex = 1;
            this.panelTop.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTop_Paint);
            this.panelTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelTop_MouseMove_1);
            // 
            // btnExt2
            // 
            this.btnExt2.Cursor = System.Windows.Forms.Cursors.No;
            this.btnExt2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExt2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnExt2.Location = new System.Drawing.Point(921, 3);
            this.btnExt2.Name = "btnExt2";
            this.btnExt2.Size = new System.Drawing.Size(48, 31);
            this.btnExt2.TabIndex = 20;
            this.btnExt2.Text = "X";
            this.btnExt2.UseVisualStyleBackColor = true;
            this.btnExt2.Click += new System.EventHandler(this.btnExt2_Click);
            // 
            // lblBookName
            // 
            this.lblBookName.AutoSize = true;
            this.lblBookName.BackColor = System.Drawing.Color.Transparent;
            this.lblBookName.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblBookName.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblBookName.Location = new System.Drawing.Point(251, 2);
            this.lblBookName.Name = "lblBookName";
            this.lblBookName.Size = new System.Drawing.Size(84, 28);
            this.lblBookName.TabIndex = 19;
            this.lblBookName.Text = "- Books";
            // 
            // lblTurnThePage
            // 
            this.lblTurnThePage.AutoSize = true;
            this.lblTurnThePage.BackColor = System.Drawing.Color.Transparent;
            this.lblTurnThePage.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTurnThePage.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblTurnThePage.Location = new System.Drawing.Point(3, 2);
            this.lblTurnThePage.Name = "lblTurnThePage";
            this.lblTurnThePage.Size = new System.Drawing.Size(252, 28);
            this.lblTurnThePage.TabIndex = 18;
            this.lblTurnThePage.Text = "Turn The Page BookStore";
            // 
            // btnExt
            // 
            this.btnExt.Cursor = System.Windows.Forms.Cursors.No;
            this.btnExt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnExt.Location = new System.Drawing.Point(688, 3);
            this.btnExt.Name = "btnExt";
            this.btnExt.Size = new System.Drawing.Size(48, 31);
            this.btnExt.TabIndex = 3;
            this.btnExt.Text = "X";
            this.btnExt.UseVisualStyleBackColor = true;
            this.btnExt.Click += new System.EventHandler(this.btnExt_Click);
            // 
            // dgwBooks
            // 
            this.dgwBooks.BackgroundColor = System.Drawing.Color.Linen;
            this.dgwBooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwBooks.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgwBooks.Location = new System.Drawing.Point(0, 39);
            this.dgwBooks.MaximumSize = new System.Drawing.Size(617, 462);
            this.dgwBooks.MinimumSize = new System.Drawing.Size(617, 462);
            this.dgwBooks.MultiSelect = false;
            this.dgwBooks.Name = "dgwBooks";
            this.dgwBooks.ReadOnly = true;
            this.dgwBooks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgwBooks.Size = new System.Drawing.Size(617, 462);
            this.dgwBooks.TabIndex = 2;
            this.dgwBooks.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgwBooks_CellClick);
            this.dgwBooks.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgwBooks_CellContentClick);
            this.dgwBooks.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgwBooks_CellDoubleClick);
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.Tan;
            this.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelect.Font = new System.Drawing.Font("Modern No. 20", 11.25F);
            this.btnSelect.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnSelect.Location = new System.Drawing.Point(632, 62);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(88, 38);
            this.btnSelect.TabIndex = 6;
            this.btnSelect.Text = "Buy";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.Tan;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnOk.Location = new System.Drawing.Point(632, 459);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(88, 38);
            this.btnOk.TabIndex = 7;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // pnlInfo
            // 
            this.pnlInfo.BackColor = System.Drawing.Color.Tan;
            this.pnlInfo.Controls.Add(this.lblName);
            this.pnlInfo.Controls.Add(this.btnInfo);
            this.pnlInfo.ForeColor = System.Drawing.Color.SaddleBrown;
            this.pnlInfo.Location = new System.Drawing.Point(626, 262);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(100, 150);
            this.pnlInfo.TabIndex = 8;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Modern No. 20", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblName.Location = new System.Drawing.Point(3, 40);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(93, 84);
            this.lblName.TabIndex = 20;
            this.lblName.Text = "You can \r\ndouble-click \r\nor select item\r\nto buy. \r\nWhen you done \r\npress Ok butto" +
    "n";
            // 
            // btnInfo
            // 
            this.btnInfo.Cursor = System.Windows.Forms.Cursors.No;
            this.btnInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnInfo.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnInfo.Location = new System.Drawing.Point(3, 3);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(93, 34);
            this.btnInfo.TabIndex = 19;
            this.btnInfo.Text = "[Close Info]";
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // pbBook
            // 
            this.pbBook.Location = new System.Drawing.Point(626, 106);
            this.pbBook.Name = "pbBook";
            this.pbBook.Size = new System.Drawing.Size(100, 150);
            this.pbBook.TabIndex = 9;
            this.pbBook.TabStop = false;
            // 
            // btnBook
            // 
            this.btnBook.BackColor = System.Drawing.Color.Tan;
            this.btnBook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBook.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBook.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnBook.Location = new System.Drawing.Point(882, 459);
            this.btnBook.Name = "btnBook";
            this.btnBook.Size = new System.Drawing.Size(88, 38);
            this.btnBook.TabIndex = 10;
            this.btnBook.Text = "Add Book";
            this.btnBook.UseVisualStyleBackColor = false;
            this.btnBook.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // lblisbn
            // 
            this.lblisbn.AutoSize = true;
            this.lblisbn.BackColor = System.Drawing.Color.Transparent;
            this.lblisbn.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblisbn.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblisbn.Location = new System.Drawing.Point(771, 187);
            this.lblisbn.Name = "lblisbn";
            this.lblisbn.Size = new System.Drawing.Size(64, 21);
            this.lblisbn.TabIndex = 19;
            this.lblisbn.Text = "ISBN:";
            // 
            // txtisbn
            // 
            this.txtisbn.BackColor = System.Drawing.Color.Tan;
            this.txtisbn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtisbn.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtisbn.ForeColor = System.Drawing.Color.Black;
            this.txtisbn.Location = new System.Drawing.Point(775, 211);
            this.txtisbn.Name = "txtisbn";
            this.txtisbn.Size = new System.Drawing.Size(177, 21);
            this.txtisbn.TabIndex = 18;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblPrice.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblPrice.Location = new System.Drawing.Point(771, 122);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(61, 21);
            this.lblPrice.TabIndex = 17;
            this.lblPrice.Text = "Price:";
            // 
            // txtPrice
            // 
            this.txtPrice.BackColor = System.Drawing.Color.Tan;
            this.txtPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrice.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrice.ForeColor = System.Drawing.Color.Black;
            this.txtPrice.Location = new System.Drawing.Point(775, 146);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(80, 21);
            this.txtPrice.TabIndex = 16;
            // 
            // lblNName
            // 
            this.lblNName.AutoSize = true;
            this.lblNName.BackColor = System.Drawing.Color.Transparent;
            this.lblNName.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNName.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblNName.Location = new System.Drawing.Point(771, 62);
            this.lblNName.Name = "lblNName";
            this.lblNName.Size = new System.Drawing.Size(63, 21);
            this.lblNName.TabIndex = 15;
            this.lblNName.Text = "Name:";
            // 
            // txtUsername
            // 
            this.txtUsername.BackColor = System.Drawing.Color.Tan;
            this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsername.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsername.ForeColor = System.Drawing.Color.Black;
            this.txtUsername.Location = new System.Drawing.Point(775, 86);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(177, 21);
            this.txtUsername.TabIndex = 14;
            // 
            // lblPage
            // 
            this.lblPage.AutoSize = true;
            this.lblPage.BackColor = System.Drawing.Color.Transparent;
            this.lblPage.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPage.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblPage.Location = new System.Drawing.Point(879, 122);
            this.lblPage.Name = "lblPage";
            this.lblPage.Size = new System.Drawing.Size(56, 21);
            this.lblPage.TabIndex = 25;
            this.lblPage.Text = "Page:";
            // 
            // txtPage
            // 
            this.txtPage.BackColor = System.Drawing.Color.Tan;
            this.txtPage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPage.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPage.ForeColor = System.Drawing.Color.Black;
            this.txtPage.Location = new System.Drawing.Point(883, 146);
            this.txtPage.Name = "txtPage";
            this.txtPage.Size = new System.Drawing.Size(69, 21);
            this.txtPage.TabIndex = 24;
            // 
            // lblPublisher
            // 
            this.lblPublisher.AutoSize = true;
            this.lblPublisher.BackColor = System.Drawing.Color.Transparent;
            this.lblPublisher.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPublisher.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblPublisher.Location = new System.Drawing.Point(771, 311);
            this.lblPublisher.Name = "lblPublisher";
            this.lblPublisher.Size = new System.Drawing.Size(100, 21);
            this.lblPublisher.TabIndex = 23;
            this.lblPublisher.Text = "Publisher:";
            // 
            // txtPublisher
            // 
            this.txtPublisher.BackColor = System.Drawing.Color.Tan;
            this.txtPublisher.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPublisher.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPublisher.ForeColor = System.Drawing.Color.Black;
            this.txtPublisher.Location = new System.Drawing.Point(775, 335);
            this.txtPublisher.Name = "txtPublisher";
            this.txtPublisher.Size = new System.Drawing.Size(177, 21);
            this.txtPublisher.TabIndex = 22;
            // 
            // lblAuthor
            // 
            this.lblAuthor.AutoSize = true;
            this.lblAuthor.BackColor = System.Drawing.Color.Transparent;
            this.lblAuthor.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAuthor.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblAuthor.Location = new System.Drawing.Point(771, 251);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(77, 21);
            this.lblAuthor.TabIndex = 21;
            this.lblAuthor.Text = "Author:";
            // 
            // txtauthor
            // 
            this.txtauthor.BackColor = System.Drawing.Color.Tan;
            this.txtauthor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtauthor.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtauthor.ForeColor = System.Drawing.Color.Black;
            this.txtauthor.Location = new System.Drawing.Point(775, 275);
            this.txtauthor.Name = "txtauthor";
            this.txtauthor.Size = new System.Drawing.Size(177, 21);
            this.txtauthor.TabIndex = 20;
            // 
            // lblDolar
            // 
            this.lblDolar.AutoSize = true;
            this.lblDolar.BackColor = System.Drawing.Color.Transparent;
            this.lblDolar.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDolar.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblDolar.Location = new System.Drawing.Point(855, 146);
            this.lblDolar.Name = "lblDolar";
            this.lblDolar.Size = new System.Drawing.Size(22, 21);
            this.lblDolar.TabIndex = 26;
            this.lblDolar.Text = "$";
            // 
            // ofdPic
            // 
            this.ofdPic.AddExtension = false;
            this.ofdPic.FileName = "openFileDialog1";
            // 
            // btnPhoto
            // 
            this.btnPhoto.BackColor = System.Drawing.Color.Tan;
            this.btnPhoto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPhoto.Font = new System.Drawing.Font("Modern No. 20", 11.25F);
            this.btnPhoto.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnPhoto.Location = new System.Drawing.Point(775, 386);
            this.btnPhoto.Name = "btnPhoto";
            this.btnPhoto.Size = new System.Drawing.Size(88, 38);
            this.btnPhoto.TabIndex = 27;
            this.btnPhoto.Text = "Picture";
            this.btnPhoto.UseVisualStyleBackColor = false;
            this.btnPhoto.Click += new System.EventHandler(this.btnPhoto_Click);
            // 
            // lblPicName
            // 
            this.lblPicName.AutoSize = true;
            this.lblPicName.BackColor = System.Drawing.Color.Transparent;
            this.lblPicName.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPicName.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblPicName.Location = new System.Drawing.Point(880, 399);
            this.lblPicName.Name = "lblPicName";
            this.lblPicName.Size = new System.Drawing.Size(0, 15);
            this.lblPicName.TabIndex = 28;
            // 
            // FormBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(982, 501);
            this.Controls.Add(this.lblPicName);
            this.Controls.Add(this.btnPhoto);
            this.Controls.Add(this.lblDolar);
            this.Controls.Add(this.lblPage);
            this.Controls.Add(this.txtPage);
            this.Controls.Add(this.lblPublisher);
            this.Controls.Add(this.txtPublisher);
            this.Controls.Add(this.lblAuthor);
            this.Controls.Add(this.txtauthor);
            this.Controls.Add(this.lblisbn);
            this.Controls.Add(this.txtisbn);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.txtPrice);
            this.Controls.Add(this.lblNName);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.btnBook);
            this.Controls.Add(this.pbBook);
            this.Controls.Add(this.pnlInfo);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.dgwBooks);
            this.Controls.Add(this.panelTop);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(982, 501);
            this.Name = "FormBook";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormBook";
            this.Load += new System.EventHandler(this.FormBook_Load);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwBooks)).EndInit();
            this.pnlInfo.ResumeLayout(false);
            this.pnlInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.booksBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.BindingSource booksBindingSource;
        internal System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Button btnExt;
        private System.Windows.Forms.DataGridViewTextBoxColumn ıDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewImageColumn pictureDataGridViewImageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn authorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dgwBooks;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.PictureBox pbBook;
        private System.Windows.Forms.Label lblBookName;
        private System.Windows.Forms.Label lblTurnThePage;
        private System.Windows.Forms.Button btnBook;
        private System.Windows.Forms.Label lblisbn;
        private System.Windows.Forms.TextBox txtisbn;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblNName;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.TextBox txtPage;
        private System.Windows.Forms.Label lblPublisher;
        private System.Windows.Forms.TextBox txtPublisher;
        private System.Windows.Forms.Label lblAuthor;
        private System.Windows.Forms.TextBox txtauthor;
        private System.Windows.Forms.Label lblDolar;
        private System.Windows.Forms.OpenFileDialog ofdPic;
        private System.Windows.Forms.Button btnPhoto;
        private System.Windows.Forms.Label lblPicName;
        private System.Windows.Forms.Button btnExt2;
    }
}