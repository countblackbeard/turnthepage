﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TurnThePage
{
    public partial class FormUserCheck : Form
    {

        SqlConnection connect;
        SqlCommand command;
        SqlDataAdapter sda;
        Customer custom;

        public FormUserCheck()
        {
            InitializeComponent();
        }

        public FormUserCheck(Customer Control)
        {
            InitializeComponent();
            custom = Control;
        }

        private void btnExt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        void fillUser()
        {
            lblNa.Text = custom.Name;
            lblEm.Text = custom.Email;
            lblAd.Text = custom.Adress;
            lblUs.Text = custom.Username;
        }

    private void btnSelect_Click(object sender, EventArgs e)
        {
            connect = new SqlConnection("Server= localhost\\sqlexpress; Database= dbTurnThePage; Integrated Security=True;");
            command = new SqlCommand("INSERT into User_info(Name,Adress,Email,Username,Password,isAdmin) values (@name,@adress,@email,@username,@password,@isadmin)", connect);
            sda = new SqlDataAdapter();



            command.Parameters.AddWithValue("@name", custom.Name);
            command.Parameters.AddWithValue("@adress", custom.Adress);
            command.Parameters.AddWithValue("@email", custom.Email);
            command.Parameters.AddWithValue("@username", custom.Username);
            command.Parameters.AddWithValue("@password", custom.Password);
            command.Parameters.AddWithValue("@isadmin", 0);

            connect.Open();
            //command.Connection = connect;
            command.ExecuteNonQuery();
            connect.Close();
            this.Close();
        }

        private void FormUserCheck_Load(object sender, EventArgs e)
        {
            fillUser();
        }


        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void panelTop_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
    }
}
