﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnThePage
{
    public class MusicCD : Product
    {

        public enum Music_type
        {
            Rock,
            Country,
            Slow,
            Pop,
            RnB
        }

        private string singer;
        private Music_type typeOfMusic;

        public string Singer { get => singer; set => singer = value; }
        internal Music_type TypeOfMusic { get => typeOfMusic; set => typeOfMusic = value; }
    }
}
