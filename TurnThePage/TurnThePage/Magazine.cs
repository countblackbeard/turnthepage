﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnThePage
{
    public class Magazine : Product
    {
        public enum Magazine_type
        {
            Actual,
            News,
            Sport,
            Gaming,
            Technology
        }
        
        private int issue;
        private Magazine_type typeOfMagazine;

        public int Issue { get => issue; set => issue = value; }
        internal Magazine_type TypeOfMagazine { get => typeOfMagazine; set => typeOfMagazine = value; }
    }
}
