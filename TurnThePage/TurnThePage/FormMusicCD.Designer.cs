﻿namespace TurnThePage
{
    partial class FormMusicCD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTop = new System.Windows.Forms.Panel();
            this.btnExt2 = new System.Windows.Forms.Button();
            this.lblMagazineName = new System.Windows.Forms.Label();
            this.lblTurnThePage = new System.Windows.Forms.Label();
            this.btnExt = new System.Windows.Forms.Button();
            this.dgwMusicCD = new System.Windows.Forms.DataGridView();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.lblName = new System.Windows.Forms.Label();
            this.btnInfo = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.lblType = new System.Windows.Forms.Label();
            this.cbType = new System.Windows.Forms.ComboBox();
            this.lblDolar = new System.Windows.Forms.Label();
            this.lblSinger = new System.Windows.Forms.Label();
            this.txtSinger = new System.Windows.Forms.TextBox();
            this.lblPrice = new System.Windows.Forms.Label();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.lblNName = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwMusicCD)).BeginInit();
            this.pnlInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.Tan;
            this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTop.Controls.Add(this.btnExt2);
            this.panelTop.Controls.Add(this.lblMagazineName);
            this.panelTop.Controls.Add(this.lblTurnThePage);
            this.panelTop.Controls.Add(this.btnExt);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(984, 39);
            this.panelTop.TabIndex = 3;
            this.panelTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelTop_MouseMove);
            // 
            // btnExt2
            // 
            this.btnExt2.Cursor = System.Windows.Forms.Cursors.No;
            this.btnExt2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExt2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnExt2.Location = new System.Drawing.Point(931, 4);
            this.btnExt2.Name = "btnExt2";
            this.btnExt2.Size = new System.Drawing.Size(48, 31);
            this.btnExt2.TabIndex = 16;
            this.btnExt2.Text = "X";
            this.btnExt2.UseVisualStyleBackColor = true;
            this.btnExt2.Click += new System.EventHandler(this.btnExt2_Click);
            // 
            // lblMagazineName
            // 
            this.lblMagazineName.AutoSize = true;
            this.lblMagazineName.BackColor = System.Drawing.Color.Transparent;
            this.lblMagazineName.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblMagazineName.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblMagazineName.Location = new System.Drawing.Point(252, 3);
            this.lblMagazineName.Name = "lblMagazineName";
            this.lblMagazineName.Size = new System.Drawing.Size(115, 28);
            this.lblMagazineName.TabIndex = 21;
            this.lblMagazineName.Text = "- MusicCD";
            // 
            // lblTurnThePage
            // 
            this.lblTurnThePage.AutoSize = true;
            this.lblTurnThePage.BackColor = System.Drawing.Color.Transparent;
            this.lblTurnThePage.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTurnThePage.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblTurnThePage.Location = new System.Drawing.Point(3, 3);
            this.lblTurnThePage.Name = "lblTurnThePage";
            this.lblTurnThePage.Size = new System.Drawing.Size(252, 28);
            this.lblTurnThePage.TabIndex = 18;
            this.lblTurnThePage.Text = "Turn The Page BookStore";
            // 
            // btnExt
            // 
            this.btnExt.Cursor = System.Windows.Forms.Cursors.No;
            this.btnExt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnExt.Location = new System.Drawing.Point(687, 3);
            this.btnExt.Name = "btnExt";
            this.btnExt.Size = new System.Drawing.Size(48, 31);
            this.btnExt.TabIndex = 3;
            this.btnExt.Text = "X";
            this.btnExt.UseVisualStyleBackColor = true;
            this.btnExt.Click += new System.EventHandler(this.btnExt_Click);
            // 
            // dgwMusicCD
            // 
            this.dgwMusicCD.BackgroundColor = System.Drawing.Color.Linen;
            this.dgwMusicCD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwMusicCD.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgwMusicCD.Location = new System.Drawing.Point(0, 39);
            this.dgwMusicCD.MultiSelect = false;
            this.dgwMusicCD.Name = "dgwMusicCD";
            this.dgwMusicCD.ReadOnly = true;
            this.dgwMusicCD.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgwMusicCD.Size = new System.Drawing.Size(617, 421);
            this.dgwMusicCD.TabIndex = 4;
            this.dgwMusicCD.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgwMusicCD_CellContentClick);
            this.dgwMusicCD.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgwMusicCD_CellDoubleClick);
            // 
            // pnlInfo
            // 
            this.pnlInfo.BackColor = System.Drawing.Color.Tan;
            this.pnlInfo.Controls.Add(this.lblName);
            this.pnlInfo.Controls.Add(this.btnInfo);
            this.pnlInfo.ForeColor = System.Drawing.Color.SaddleBrown;
            this.pnlInfo.Location = new System.Drawing.Point(634, 145);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(100, 150);
            this.pnlInfo.TabIndex = 15;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Modern No. 20", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblName.Location = new System.Drawing.Point(3, 51);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(93, 84);
            this.lblName.TabIndex = 20;
            this.lblName.Text = "You can \r\ndouble-click \r\nor select item\r\nto buy. \r\nWhen you done \r\npress Ok butto" +
    "n";
            // 
            // btnInfo
            // 
            this.btnInfo.Cursor = System.Windows.Forms.Cursors.No;
            this.btnInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnInfo.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnInfo.Location = new System.Drawing.Point(3, 3);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(93, 34);
            this.btnInfo.TabIndex = 19;
            this.btnInfo.Text = "[Close Info]";
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.Tan;
            this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnOk.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnOk.Location = new System.Drawing.Point(640, 413);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(88, 38);
            this.btnOk.TabIndex = 14;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = false;
            // 
            // btnSelect
            // 
            this.btnSelect.BackColor = System.Drawing.Color.Tan;
            this.btnSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnSelect.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnSelect.Location = new System.Drawing.Point(640, 55);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(88, 38);
            this.btnSelect.TabIndex = 13;
            this.btnSelect.Text = "Select";
            this.btnSelect.UseVisualStyleBackColor = false;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.BackColor = System.Drawing.Color.Transparent;
            this.lblType.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblType.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblType.Location = new System.Drawing.Point(771, 322);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(57, 21);
            this.lblType.TabIndex = 46;
            this.lblType.Text = "Type:";
            // 
            // cbType
            // 
            this.cbType.BackColor = System.Drawing.Color.Tan;
            this.cbType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbType.FormattingEnabled = true;
            this.cbType.Items.AddRange(new object[] {
            "Rock",
            "Country",
            "Slow",
            "Pop",
            "RnB"});
            this.cbType.Location = new System.Drawing.Point(775, 346);
            this.cbType.Name = "cbType";
            this.cbType.Size = new System.Drawing.Size(177, 21);
            this.cbType.TabIndex = 45;
            // 
            // lblDolar
            // 
            this.lblDolar.AutoSize = true;
            this.lblDolar.BackColor = System.Drawing.Color.Transparent;
            this.lblDolar.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDolar.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblDolar.Location = new System.Drawing.Point(855, 205);
            this.lblDolar.Name = "lblDolar";
            this.lblDolar.Size = new System.Drawing.Size(22, 21);
            this.lblDolar.TabIndex = 44;
            this.lblDolar.Text = "$";
            // 
            // lblSinger
            // 
            this.lblSinger.AutoSize = true;
            this.lblSinger.BackColor = System.Drawing.Color.Transparent;
            this.lblSinger.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSinger.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblSinger.Location = new System.Drawing.Point(771, 250);
            this.lblSinger.Name = "lblSinger";
            this.lblSinger.Size = new System.Drawing.Size(70, 21);
            this.lblSinger.TabIndex = 43;
            this.lblSinger.Text = "Singer:";
            // 
            // txtSinger
            // 
            this.txtSinger.BackColor = System.Drawing.Color.Tan;
            this.txtSinger.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSinger.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSinger.ForeColor = System.Drawing.Color.Black;
            this.txtSinger.Location = new System.Drawing.Point(775, 274);
            this.txtSinger.Name = "txtSinger";
            this.txtSinger.Size = new System.Drawing.Size(177, 21);
            this.txtSinger.TabIndex = 42;
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.BackColor = System.Drawing.Color.Transparent;
            this.lblPrice.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrice.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblPrice.Location = new System.Drawing.Point(771, 181);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(61, 21);
            this.lblPrice.TabIndex = 41;
            this.lblPrice.Text = "Price:";
            // 
            // txtPrice
            // 
            this.txtPrice.BackColor = System.Drawing.Color.Tan;
            this.txtPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPrice.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrice.ForeColor = System.Drawing.Color.Black;
            this.txtPrice.Location = new System.Drawing.Point(775, 205);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(80, 21);
            this.txtPrice.TabIndex = 40;
            // 
            // lblNName
            // 
            this.lblNName.AutoSize = true;
            this.lblNName.BackColor = System.Drawing.Color.Transparent;
            this.lblNName.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNName.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblNName.Location = new System.Drawing.Point(771, 121);
            this.lblNName.Name = "lblNName";
            this.lblNName.Size = new System.Drawing.Size(63, 21);
            this.lblNName.TabIndex = 39;
            this.lblNName.Text = "Name:";
            // 
            // txtUsername
            // 
            this.txtUsername.BackColor = System.Drawing.Color.Tan;
            this.txtUsername.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsername.Font = new System.Drawing.Font("Modern No. 20", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsername.ForeColor = System.Drawing.Color.Black;
            this.txtUsername.Location = new System.Drawing.Point(775, 145);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(177, 21);
            this.txtUsername.TabIndex = 38;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.Tan;
            this.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdd.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnAdd.Location = new System.Drawing.Point(884, 415);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(88, 38);
            this.btnAdd.TabIndex = 47;
            this.btnAdd.Text = "Add CD";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // FormMusicCD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.ClientSize = new System.Drawing.Size(984, 460);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.cbType);
            this.Controls.Add(this.lblDolar);
            this.Controls.Add(this.lblSinger);
            this.Controls.Add(this.txtSinger);
            this.Controls.Add(this.lblPrice);
            this.Controls.Add(this.txtPrice);
            this.Controls.Add(this.lblNName);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.pnlInfo);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.dgwMusicCD);
            this.Controls.Add(this.panelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(984, 460);
            this.Name = "FormMusicCD";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormMusicCD";
            this.Load += new System.EventHandler(this.FormMusicCD_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FormMusicCD_MouseMove);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgwMusicCD)).EndInit();
            this.pnlInfo.ResumeLayout(false);
            this.pnlInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Button btnExt;
        private System.Windows.Forms.Label lblTurnThePage;
        private System.Windows.Forms.DataGridView dgwMusicCD;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.Label lblMagazineName;
        private System.Windows.Forms.Button btnExt2;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.ComboBox cbType;
        private System.Windows.Forms.Label lblDolar;
        private System.Windows.Forms.Label lblSinger;
        private System.Windows.Forms.TextBox txtSinger;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.Label lblNName;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Button btnAdd;
    }
}