﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TurnThePage
{
    public partial class FormBuy : Form
    {
        Book book;
        Magazine magazine;
        MusicCD musicCD;
        public ShoppingCart shoppingCart = new ShoppingCart();
        ItemToPurchase itemToPurchase = new ItemToPurchase();
        int productType = 0;

        public FormBuy()
        {
            InitializeComponent();
        }

        public FormBuy(Book boo, ShoppingCart shopping)
        {
            InitializeComponent();

            book = new Book();
            book = boo;
            shoppingCart = shopping;

            lblName.Text += " " + book.Name;
            lblPrice.Text = book.Price.ToString();
            productType = 0;
        }

        public FormBuy(Magazine magazin, ShoppingCart shopping)
        {
            InitializeComponent();

            magazine = new Magazine();
            magazine = magazin;
            shoppingCart = shopping;

            lblName.Text += " " + magazine.Name;
            lblPrice.Text = magazine.Price.ToString();
            productType = 1;
        }

        public FormBuy(MusicCD musicC, ShoppingCart shopping)
        {
            InitializeComponent();

            musicCD = new MusicCD();
            musicCD = musicC;
            shoppingCart = shopping;

            lblName.Text += " " + musicCD.Name;
            lblPrice.Text = musicCD.Price.ToString();
            productType = 2;
        }

        private void btnExt_Click(object sender, EventArgs e)
        {
            DialogResult a = MessageBox.Show("Do you wanna close this program?", "Thank you for choosing us!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (a == DialogResult.Yes)
                this.Close();
        }

        private void panelTop_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void lblPlus_Click(object sender, EventArgs e)
        {
           switch(productType)
            {
                case 0:
                    txtQuantity.Text = (Int32.Parse(txtQuantity.Text) + 1).ToString();
                    lblPrice.Text = (book.Price * Int32.Parse(txtQuantity.Text)).ToString();
                    break;
                case 1:
                    txtQuantity.Text = (Int32.Parse(txtQuantity.Text) + 1).ToString();
                    lblPrice.Text = (magazine.Price * Int32.Parse(txtQuantity.Text)).ToString();
                    break;
                case 2:
                    txtQuantity.Text = (Int32.Parse(txtQuantity.Text) + 1).ToString();
                    lblPrice.Text = (musicCD.Price * Int32.Parse(txtQuantity.Text)).ToString();
                    break;
                default:
                    break;
            }
            
        }

        private void lblMinus_Click(object sender, EventArgs e)
        {
            switch (productType)
            {
                case 0:
                    txtQuantity.Text = (Int32.Parse(txtQuantity.Text) - 1).ToString();
                    lblPrice.Text = (book.Price * Int32.Parse(txtQuantity.Text)).ToString();
                    break;
                case 1:
                    txtQuantity.Text = (Int32.Parse(txtQuantity.Text) - 1).ToString();
                    lblPrice.Text = (magazine.Price * Int32.Parse(txtQuantity.Text)).ToString();
                    break;
                case 2:
                    txtQuantity.Text = (Int32.Parse(txtQuantity.Text) - 1).ToString();
                    lblPrice.Text = (musicCD.Price * Int32.Parse(txtQuantity.Text)).ToString();
                    break;
                default:
                    break;
            }
        }

        private void FormBuy_Load(object sender, EventArgs e)
        {
          
        }

        private void lblName_Click(object sender, EventArgs e)
        {

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblPrice_Click(object sender, EventArgs e)
        {

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            switch (productType)
            {
                case 0:
                    itemToPurchase.Product = book;
                    itemToPurchase.Quantity = Int32.Parse(txtQuantity.Text);
                    shoppingCart.addProduct(itemToPurchase);
                    break;
                case 1:
                    itemToPurchase.Product = magazine;
                    itemToPurchase.Quantity = Int32.Parse(txtQuantity.Text);
                    shoppingCart.addProduct(itemToPurchase);
                    break;
                case 2:
                    itemToPurchase.Product = musicCD;
                    itemToPurchase.Quantity = Int32.Parse(txtQuantity.Text);
                    shoppingCart.addProduct(itemToPurchase);
                    break;
                default:
                    break;
            }

            
        }
    }
}
