﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TurnThePage
{
    public partial class FormMain : Form
    {
        int isBuyed = 0;
        ShoppingCart shoppingCart = new ShoppingCart();
        User user;
        Customer custom;
        public bool Admin =false;
        public bool log;

        public FormMain()
        {
            InitializeComponent();
           
        }

        public FormMain(User user,Customer control)
        {
            InitializeComponent();
            lblName.Text = user.Name;
            lblMoney.Text = shoppingCart.PaymentAmount.ToString();
            shoppingCart.CustomerId1 = user.CustomerID;
            custom = control;
            this.user = user;
            
        }


        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ExStyle |= 0x02000000;   // WS_EX_COMPOSITED
                return cp;
            }
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            pbLogo.BorderStyle = BorderStyle.None;
            pbShopCart.BorderStyle = BorderStyle.None;

            AdminUser a = new AdminUser();
            Type t = a.GetType();
            if (t == user.GetType())
            {
                if (custom != null)
                    btnUser.Visible = true;
                Admin = true;
                pnlCart.Visible = false;
            }
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void Book_Click(object sender, EventArgs e)
        {

       
        }

        private void btnExt_Click(object sender, EventArgs e)
        {
            log = true;
            DialogResult a = MessageBox.Show("Do you wanna close this program?", "Thank you for choosing us!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
           
            if (a == DialogResult.Yes)
                System.Windows.Forms.Application.Exit();
        }

        private void pbBook_Click(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void lblBooks_Click(object sender, EventArgs e)
        {
            using (FormBook fb = new FormBook(shoppingCart, Admin))
            {
                if (fb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    shoppingCart = fb.shoppingCart;
                }
            }
        }

        private void pbBooks_Click(object sender, EventArgs e)
        {
            using (FormBook fb = new FormBook(shoppingCart,Admin))
            {
                if (fb.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    shoppingCart = fb.shoppingCart;
                }
            }          
            
        }

        private void panelTop_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void pbMagazine_Click(object sender, EventArgs e)
        {
            using (FormMagazine fm = new FormMagazine(shoppingCart,Admin))
            {
                if (fm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    shoppingCart = fm.shoppingCart;
                }
            }
        }

        private void pcMusicCd_Click(object sender, EventArgs e)
        {
            using (FormMusicCD fmc = new FormMusicCD(shoppingCart,Admin))
            {
                if (fmc.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    shoppingCart = fmc.shoppingCart;
                }
            }
            
        }

        private void lblMagazine_Click(object sender, EventArgs e)
        {
            using (FormMagazine fm = new FormMagazine(shoppingCart, Admin))
            {
                if (fm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    shoppingCart = fm.shoppingCart;
                }
            }
        }

        private void lblMusicCd_Click(object sender, EventArgs e)
        {
            using (FormMagazine fm = new FormMagazine(shoppingCart,Admin))
            {
                if (fm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    shoppingCart = fm.shoppingCart;
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pbShopCart_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void timerMain_Tick(object sender, EventArgs e)
        {
            lblMoney.Text = shoppingCart.PaymentAmount.ToString();

            if (lblMoney.Text != "0")
            {
                int item = 0;
                int i = 0;

                foreach (ItemToPurchase myItem in shoppingCart.ItemsToPurchase)
                    item += myItem.Quantity;




                lblExpo.Text = "Your shopping cart have " + item + " items.";
                lblExpo.Visible = true;
                btnPay.Visible = true;
            }
            else
            {
                lblExpo.Visible = false;
                btnPay.Visible = false;
            }
          

        }

        private void btnPay_Click(object sender, EventArgs e)
        {
            using (FormCart fc = new FormCart(shoppingCart))
            {
                if (fc.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    shoppingCart = fc.shoppingCart;
                    isBuyed = fc.buy;

                    if (isBuyed == 1)
                    {
                        pnlInfo.Visible = true;
                    }
                    else if (isBuyed == 2)
                    {
                        pnlNeg.Visible = true;
                    }
                }
            }
        }

        private void btnNeg_Click(object sender, EventArgs e)
        {
            pnlNeg.Visible = false;
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            pnlInfo.Visible = false;
        }

        private void btnUser_Click(object sender, EventArgs e)
        {
            using (FormUserCheck fuc = new FormUserCheck(custom))
            {
                if (fuc.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                
                }
            }
                btnUser.Visible = false;
        }

        private void btnLogut_Click(object sender, EventArgs e)
        {
            log = false;
            DialogResult a = MessageBox.Show("Do you wanna logout?", "Thank you for choosing us!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
           
            if (a == DialogResult.Yes)
                this.Close();
        }
    }
}
