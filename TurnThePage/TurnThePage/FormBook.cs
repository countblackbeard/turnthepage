﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TurnThePage
{


    public partial class FormBook : Form
    {
        public ShoppingCart shoppingCart = new ShoppingCart();
        SqlConnection connect;
        SqlCommand command;
        SqlDataAdapter sda;
        Book temp;
        bool Admin;

        public FormBook()
        {
            InitializeComponent();
        }

        public FormBook(ShoppingCart shopping, bool admin)
        {
            InitializeComponent();
            shoppingCart = shopping;
            Admin = admin;
        }

        List<Book> getBooks()
        {
            List<Book> bookList = new List<Book>();

            connect = new SqlConnection("Server= localhost\\sqlexpress; Database= dbTurnThePage; Integrated Security=True;");
            command = new SqlCommand();
            sda = new SqlDataAdapter();
            command.CommandText = "SELECT *FROM Book_info";
            command.CommandType = CommandType.Text;
            
            connect.Open();
            command.Connection = connect;
            SqlDataReader dr = command.ExecuteReader();

            while (dr.Read())
            {
                temp = new Book();
                temp.Name = dr[0].ToString();
                temp.ID1 = Convert.ToInt32(dr[1]);
                temp.Price = (float)Convert.ToDouble(dr[2]);
                temp.Isbn = Convert.ToInt32(dr[3]);
                temp.Author = dr[4].ToString();
                temp.Publisher = dr[5].ToString();
                temp.Page = Convert.ToInt32(dr[6]);
                temp.Cover_page_picture = dr[7].ToString();

                bookList.Add(temp);

            }
            connect.Close();

            return bookList;
        }

        void fillDataGrid ()
        {

            dgwBooks.Rows.Clear();
            dgwBooks.Refresh();

            int i = 0;

            List<Book> bookList = new List<Book>();
            bookList = getBooks();

            dgwBooks.ColumnCount = 8;
            dgwBooks.Columns[0].Name = "ID";
            dgwBooks.Columns[7].Name = "Picture";
            dgwBooks.Columns[2].Name = "Price";
            dgwBooks.Columns[1].Name = "Name";
            dgwBooks.Columns[3].Name = "Author";
            dgwBooks.Columns[4].Name = "Publisher";
            dgwBooks.Columns[5].Name = "Page";
            dgwBooks.Columns[6].Name = "Isbn";
            dgwBooks.Columns[7].Visible = false;

            dgwBooks.BorderStyle = BorderStyle.None;
            dgwBooks.AlternatingRowsDefaultCellStyle.BackColor = Color.Tan;
            dgwBooks.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dgwBooks.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dgwBooks.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dgwBooks.BackgroundColor = Color.White;

            dgwBooks.EnableHeadersVisualStyles = false;
            dgwBooks.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dgwBooks.ColumnHeadersDefaultCellStyle.BackColor = Color.SaddleBrown;
            dgwBooks.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

            dgwBooks.Columns[0].Width = 20;
            dgwBooks.Columns[2].Width = 30;
            dgwBooks.Columns[5].Width = 30;

            while (i < bookList.Count)
            {                
                string[] row = new string[] {bookList[i].ID1.ToString(), bookList[i].Name, bookList[i].Price.ToString(),
                     bookList[i].Author, bookList[i].Publisher, bookList[i].Page.ToString(), bookList[i].Isbn.ToString(), bookList[i].Cover_page_picture.ToString()};
                dgwBooks.Rows.Add(row);
                i++;

                
            }
        }

        private void FormBook_Load(object sender, EventArgs e)
        {
            fillDataGrid();
            
            if(Admin == false)
            {
                this.Size = new Size(750, 501);
            }
            else
            {
                //this.Size = new Size(1085, 501);
                btnExt.Visible = false;
                btnSelect.Visible = false;
            }
            
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void Book_Click(object sender, EventArgs e)
        {

            FormBook fb = new FormBook();
            fb.Show();
        }

        private void dtgvBooks_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }


        private void btnExt_Click(object sender, EventArgs e)
        {           
                this.Close();
        }

        private void panelTop_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void panelTop_MouseDown(object sender, MouseEventArgs e)
        {

        }

        private void panelTop_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dtgvBooks_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void panelTop_MouseMove_1(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void dtgvBooks_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgwBooks_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgwBooks_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void dgwBooks_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewRow row in dgwBooks.SelectedRows)
            {
                pbBook.Image = Image.FromFile(row.Cells[7].Value.ToString());
            }
        }

        private void dgwBooks_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (Admin == false)
            {
                foreach (DataGridViewRow row in dgwBooks.SelectedRows)
                {
                    Book temp = new Book();
                    //row.Cells[1].Value.ToString();
                    temp.ID1 = Convert.ToInt32(row.Cells[0].Value);
                    // temp.Cover_page_picture = row.Cells[1].Value.ToString();
                    temp.Price = Convert.ToSingle(row.Cells[2].Value);
                    temp.Name = row.Cells[1].Value.ToString();
                    temp.Author = row.Cells[3].Value.ToString();
                    temp.Publisher = row.Cells[4].Value.ToString();
                    temp.Page = Convert.ToInt32(row.Cells[5].Value);
                    temp.Isbn = Convert.ToInt32(row.Cells[6].Value);

                    using (FormBuy formBuy = new FormBuy(temp, shoppingCart))
                    {
                        if (formBuy.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            shoppingCart = formBuy.shoppingCart;
                        }
                    }
                }
            }
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            pnlInfo.Visible = false;
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgwBooks.SelectedRows)
            {
                Book temp = new Book();

                temp.ID1 = Convert.ToInt32(row.Cells[0].Value);
                //temp.Cover_page_picture = row.Cells[1].Value.ToString();
                temp.Price = Convert.ToSingle(row.Cells[2].Value);
                temp.Name = row.Cells[1].Value.ToString();
                temp.Author = row.Cells[3].Value.ToString();
                temp.Publisher = row.Cells[4].Value.ToString();
                temp.Page = Convert.ToInt32(row.Cells[5].Value);
                temp.Isbn = Convert.ToInt32(row.Cells[6].Value);

                using (FormBuy formBuy = new FormBuy(temp, shoppingCart))
                {
                    if (formBuy.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        shoppingCart = formBuy.shoppingCart;
                    }
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {

        }

        void addBook()
        {
            connect = new SqlConnection("Server= localhost\\sqlexpress; Database= dbTurnThePage; Integrated Security=True;");
            command = new SqlCommand("INSERT into Book_info(name,price,isbn,author,publisher,page,cover_page_picture) values (@name,@price,@isbn,@author,@publisher,@page,@cover_page_picture)", connect);
            sda = new SqlDataAdapter();



            command.Parameters.AddWithValue("@name", txtUsername.Text);
            command.Parameters.AddWithValue("@price", txtPrice.Text);
            command.Parameters.AddWithValue("@isbn", txtisbn.Text);
            command.Parameters.AddWithValue("@author", txtauthor.Text);
            command.Parameters.AddWithValue("@publisher", txtPublisher.Text);
            command.Parameters.AddWithValue("@page", txtPage.Text);
            command.Parameters.AddWithValue("@cover_page_picture", ofdPic.FileName);

            connect.Open();
            //command.Connection = connect;
            command.ExecuteNonQuery();
            connect.Close();
            clearTxt();
        }

        void clearTxt()
        {
            txtPage.Text = "";
            txtauthor.Text = "";
            txtisbn.Text = "";
            txtPrice.Text = "";
            txtPublisher.Text = "";
            txtUsername.Text = "";
            ofdPic.Dispose();
            lblPicName.Text = "";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            addBook();
            fillDataGrid();
        }

        private void btnPhoto_Click(object sender, EventArgs e)
        {
            ofdPic.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            ofdPic.Title = "Choose a image file...";

            if (ofdPic.ShowDialog() == DialogResult.OK)
            {
                lblPicName.Text = ofdPic.SafeFileName;
                lblPicName.Visible = true;
            }
        }

        private void btnExt2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }


}

