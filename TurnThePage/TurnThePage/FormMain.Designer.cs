﻿namespace TurnThePage
{
    partial class FormMain
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.btnExt = new System.Windows.Forms.Button();
            this.pbMagazine = new System.Windows.Forms.PictureBox();
            this.pcMusicCd = new System.Windows.Forms.PictureBox();
            this.panelTop = new System.Windows.Forms.Panel();
            this.lblTurnThePage = new System.Windows.Forms.Label();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.pbBooks = new System.Windows.Forms.PictureBox();
            this.lblBooks = new System.Windows.Forms.Label();
            this.pnlBooks = new System.Windows.Forms.Panel();
            this.pnlMusicCd = new System.Windows.Forms.Panel();
            this.lblMusicCd = new System.Windows.Forms.Label();
            this.pnlMagazine = new System.Windows.Forms.Panel();
            this.lblMagazine = new System.Windows.Forms.Label();
            this.pnlUser = new System.Windows.Forms.Panel();
            this.lblWelcome = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblMoney = new System.Windows.Forms.Label();
            this.pbShopCart = new System.Windows.Forms.PictureBox();
            this.lblDolar = new System.Windows.Forms.Label();
            this.pnlCart = new System.Windows.Forms.Panel();
            this.btnPay = new System.Windows.Forms.Button();
            this.lblExpo = new System.Windows.Forms.Label();
            this.timerMain = new System.Windows.Forms.Timer(this.components);
            this.pnlNeg = new System.Windows.Forms.Panel();
            this.btnNeg = new System.Windows.Forms.Button();
            this.lblCancel = new System.Windows.Forms.Label();
            this.pnlInfo = new System.Windows.Forms.Panel();
            this.btnInfo = new System.Windows.Forms.Button();
            this.lblTrue = new System.Windows.Forms.Label();
            this.btnUser = new System.Windows.Forms.Button();
            this.btnLogut = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbMagazine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcMusicCd)).BeginInit();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBooks)).BeginInit();
            this.pnlBooks.SuspendLayout();
            this.pnlMusicCd.SuspendLayout();
            this.pnlMagazine.SuspendLayout();
            this.pnlUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbShopCart)).BeginInit();
            this.pnlCart.SuspendLayout();
            this.pnlNeg.SuspendLayout();
            this.pnlInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnExt
            // 
            this.btnExt.BackColor = System.Drawing.Color.Transparent;
            this.btnExt.Cursor = System.Windows.Forms.Cursors.No;
            this.btnExt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnExt.Location = new System.Drawing.Point(747, 3);
            this.btnExt.Name = "btnExt";
            this.btnExt.Size = new System.Drawing.Size(48, 31);
            this.btnExt.TabIndex = 4;
            this.btnExt.Text = "X";
            this.btnExt.UseVisualStyleBackColor = false;
            this.btnExt.Click += new System.EventHandler(this.btnExt_Click);
            // 
            // pbMagazine
            // 
            this.pbMagazine.BackColor = System.Drawing.Color.Transparent;
            this.pbMagazine.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbMagazine.BackgroundImage")));
            this.pbMagazine.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbMagazine.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbMagazine.Location = new System.Drawing.Point(17, 12);
            this.pbMagazine.Name = "pbMagazine";
            this.pbMagazine.Size = new System.Drawing.Size(60, 60);
            this.pbMagazine.TabIndex = 6;
            this.pbMagazine.TabStop = false;
            this.pbMagazine.Click += new System.EventHandler(this.pbMagazine_Click);
            // 
            // pcMusicCd
            // 
            this.pcMusicCd.BackColor = System.Drawing.Color.Transparent;
            this.pcMusicCd.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pcMusicCd.BackgroundImage")));
            this.pcMusicCd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pcMusicCd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pcMusicCd.Location = new System.Drawing.Point(18, 11);
            this.pcMusicCd.Name = "pcMusicCd";
            this.pcMusicCd.Size = new System.Drawing.Size(60, 60);
            this.pcMusicCd.TabIndex = 7;
            this.pcMusicCd.TabStop = false;
            this.pcMusicCd.Click += new System.EventHandler(this.pcMusicCd_Click);
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.Tan;
            this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTop.Controls.Add(this.btnLogut);
            this.panelTop.Controls.Add(this.lblTurnThePage);
            this.panelTop.Controls.Add(this.pbLogo);
            this.panelTop.Controls.Add(this.btnExt);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(808, 39);
            this.panelTop.TabIndex = 8;
            this.panelTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelTop_MouseMove);
            // 
            // lblTurnThePage
            // 
            this.lblTurnThePage.AutoSize = true;
            this.lblTurnThePage.BackColor = System.Drawing.Color.Transparent;
            this.lblTurnThePage.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTurnThePage.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblTurnThePage.Location = new System.Drawing.Point(60, 2);
            this.lblTurnThePage.Name = "lblTurnThePage";
            this.lblTurnThePage.Size = new System.Drawing.Size(252, 28);
            this.lblTurnThePage.TabIndex = 17;
            this.lblTurnThePage.Text = "Turn The Page BookStore";
            // 
            // pbLogo
            // 
            this.pbLogo.BackColor = System.Drawing.Color.Transparent;
            this.pbLogo.BackgroundImage = global::TurnThePage.Properties.Resources.logo;
            this.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbLogo.Location = new System.Drawing.Point(11, 3);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(33, 30);
            this.pbLogo.TabIndex = 16;
            this.pbLogo.TabStop = false;
            this.pbLogo.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pbBooks
            // 
            this.pbBooks.BackColor = System.Drawing.Color.Transparent;
            this.pbBooks.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pbBooks.BackgroundImage")));
            this.pbBooks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbBooks.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbBooks.Location = new System.Drawing.Point(18, 15);
            this.pbBooks.Name = "pbBooks";
            this.pbBooks.Size = new System.Drawing.Size(60, 60);
            this.pbBooks.TabIndex = 5;
            this.pbBooks.TabStop = false;
            this.pbBooks.Click += new System.EventHandler(this.pbBooks_Click);
            // 
            // lblBooks
            // 
            this.lblBooks.AutoSize = true;
            this.lblBooks.BackColor = System.Drawing.Color.Transparent;
            this.lblBooks.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBooks.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblBooks.Location = new System.Drawing.Point(94, 29);
            this.lblBooks.Name = "lblBooks";
            this.lblBooks.Size = new System.Drawing.Size(91, 31);
            this.lblBooks.TabIndex = 9;
            this.lblBooks.Text = "Books";
            this.lblBooks.Click += new System.EventHandler(this.lblBooks_Click);
            // 
            // pnlBooks
            // 
            this.pnlBooks.BackColor = System.Drawing.Color.Tan;
            this.pnlBooks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBooks.Controls.Add(this.pbBooks);
            this.pnlBooks.Controls.Add(this.lblBooks);
            this.pnlBooks.Location = new System.Drawing.Point(21, 183);
            this.pnlBooks.Name = "pnlBooks";
            this.pnlBooks.Size = new System.Drawing.Size(244, 89);
            this.pnlBooks.TabIndex = 12;
            // 
            // pnlMusicCd
            // 
            this.pnlMusicCd.BackColor = System.Drawing.Color.Tan;
            this.pnlMusicCd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMusicCd.Controls.Add(this.lblMusicCd);
            this.pnlMusicCd.Controls.Add(this.pcMusicCd);
            this.pnlMusicCd.Location = new System.Drawing.Point(543, 183);
            this.pnlMusicCd.Name = "pnlMusicCd";
            this.pnlMusicCd.Size = new System.Drawing.Size(244, 89);
            this.pnlMusicCd.TabIndex = 12;
            // 
            // lblMusicCd
            // 
            this.lblMusicCd.AutoSize = true;
            this.lblMusicCd.BackColor = System.Drawing.Color.Transparent;
            this.lblMusicCd.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMusicCd.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblMusicCd.Location = new System.Drawing.Point(94, 29);
            this.lblMusicCd.Name = "lblMusicCd";
            this.lblMusicCd.Size = new System.Drawing.Size(148, 31);
            this.lblMusicCd.TabIndex = 11;
            this.lblMusicCd.Text = "Music CDs";
            this.lblMusicCd.Click += new System.EventHandler(this.lblMusicCd_Click);
            // 
            // pnlMagazine
            // 
            this.pnlMagazine.BackColor = System.Drawing.Color.Tan;
            this.pnlMagazine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlMagazine.Controls.Add(this.lblMagazine);
            this.pnlMagazine.Controls.Add(this.pbMagazine);
            this.pnlMagazine.Location = new System.Drawing.Point(282, 183);
            this.pnlMagazine.Name = "pnlMagazine";
            this.pnlMagazine.Size = new System.Drawing.Size(244, 89);
            this.pnlMagazine.TabIndex = 13;
            // 
            // lblMagazine
            // 
            this.lblMagazine.AutoSize = true;
            this.lblMagazine.BackColor = System.Drawing.Color.Transparent;
            this.lblMagazine.Font = new System.Drawing.Font("Modern No. 20", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMagazine.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblMagazine.Location = new System.Drawing.Point(93, 25);
            this.lblMagazine.Name = "lblMagazine";
            this.lblMagazine.Size = new System.Drawing.Size(146, 31);
            this.lblMagazine.TabIndex = 10;
            this.lblMagazine.Text = "Magazines";
            this.lblMagazine.Click += new System.EventHandler(this.lblMagazine_Click);
            // 
            // pnlUser
            // 
            this.pnlUser.BackColor = System.Drawing.Color.Tan;
            this.pnlUser.Controls.Add(this.lblWelcome);
            this.pnlUser.Controls.Add(this.lblName);
            this.pnlUser.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlUser.Location = new System.Drawing.Point(0, 417);
            this.pnlUser.Name = "pnlUser";
            this.pnlUser.Size = new System.Drawing.Size(808, 36);
            this.pnlUser.TabIndex = 15;
            // 
            // lblWelcome
            // 
            this.lblWelcome.AutoSize = true;
            this.lblWelcome.BackColor = System.Drawing.Color.Transparent;
            this.lblWelcome.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWelcome.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblWelcome.Location = new System.Drawing.Point(539, 9);
            this.lblWelcome.Name = "lblWelcome";
            this.lblWelcome.Size = new System.Drawing.Size(77, 18);
            this.lblWelcome.TabIndex = 16;
            this.lblWelcome.Text = "Welcome!";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.BackColor = System.Drawing.Color.Transparent;
            this.lblName.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblName.Location = new System.Drawing.Point(622, 9);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(49, 18);
            this.lblName.TabIndex = 17;
            this.lblName.Text = "Guest";
            // 
            // lblMoney
            // 
            this.lblMoney.AutoSize = true;
            this.lblMoney.BackColor = System.Drawing.Color.Transparent;
            this.lblMoney.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMoney.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblMoney.Location = new System.Drawing.Point(44, 3);
            this.lblMoney.Name = "lblMoney";
            this.lblMoney.Size = new System.Drawing.Size(36, 21);
            this.lblMoney.TabIndex = 18;
            this.lblMoney.Text = "0,0";
            // 
            // pbShopCart
            // 
            this.pbShopCart.BackColor = System.Drawing.Color.Transparent;
            this.pbShopCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbShopCart.Image = global::TurnThePage.Properties.Resources.shoppingCart;
            this.pbShopCart.Location = new System.Drawing.Point(3, 3);
            this.pbShopCart.Name = "pbShopCart";
            this.pbShopCart.Size = new System.Drawing.Size(25, 25);
            this.pbShopCart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbShopCart.TabIndex = 19;
            this.pbShopCart.TabStop = false;
            this.pbShopCart.Click += new System.EventHandler(this.pbShopCart_Click);
            // 
            // lblDolar
            // 
            this.lblDolar.AutoSize = true;
            this.lblDolar.BackColor = System.Drawing.Color.Transparent;
            this.lblDolar.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDolar.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblDolar.Location = new System.Drawing.Point(116, 3);
            this.lblDolar.Name = "lblDolar";
            this.lblDolar.Size = new System.Drawing.Size(22, 21);
            this.lblDolar.TabIndex = 20;
            this.lblDolar.Text = "$";
            // 
            // pnlCart
            // 
            this.pnlCart.BackColor = System.Drawing.Color.Tan;
            this.pnlCart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCart.Controls.Add(this.pbShopCart);
            this.pnlCart.Controls.Add(this.lblDolar);
            this.pnlCart.Controls.Add(this.lblMoney);
            this.pnlCart.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlCart.Location = new System.Drawing.Point(648, 45);
            this.pnlCart.Name = "pnlCart";
            this.pnlCart.Size = new System.Drawing.Size(148, 33);
            this.pnlCart.TabIndex = 18;
            this.pnlCart.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btnPay
            // 
            this.btnPay.BackColor = System.Drawing.Color.Tan;
            this.btnPay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPay.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnPay.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnPay.Location = new System.Drawing.Point(562, 50);
            this.btnPay.Name = "btnPay";
            this.btnPay.Size = new System.Drawing.Size(80, 23);
            this.btnPay.TabIndex = 21;
            this.btnPay.Text = "Examine/Buy";
            this.btnPay.UseVisualStyleBackColor = false;
            this.btnPay.Visible = false;
            this.btnPay.Click += new System.EventHandler(this.btnPay_Click);
            // 
            // lblExpo
            // 
            this.lblExpo.AutoSize = true;
            this.lblExpo.BackColor = System.Drawing.Color.Transparent;
            this.lblExpo.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExpo.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblExpo.Location = new System.Drawing.Point(222, 314);
            this.lblExpo.Name = "lblExpo";
            this.lblExpo.Size = new System.Drawing.Size(21, 24);
            this.lblExpo.TabIndex = 10;
            this.lblExpo.Text = "b";
            this.lblExpo.Visible = false;
            // 
            // timerMain
            // 
            this.timerMain.Enabled = true;
            this.timerMain.Tick += new System.EventHandler(this.timerMain_Tick);
            // 
            // pnlNeg
            // 
            this.pnlNeg.BackColor = System.Drawing.Color.Tan;
            this.pnlNeg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlNeg.Controls.Add(this.btnNeg);
            this.pnlNeg.Controls.Add(this.lblCancel);
            this.pnlNeg.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlNeg.Location = new System.Drawing.Point(40, 68);
            this.pnlNeg.Name = "pnlNeg";
            this.pnlNeg.Size = new System.Drawing.Size(264, 95);
            this.pnlNeg.TabIndex = 25;
            this.pnlNeg.Visible = false;
            // 
            // btnNeg
            // 
            this.btnNeg.Cursor = System.Windows.Forms.Cursors.No;
            this.btnNeg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNeg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnNeg.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnNeg.Location = new System.Drawing.Point(73, 56);
            this.btnNeg.Name = "btnNeg";
            this.btnNeg.Size = new System.Drawing.Size(93, 34);
            this.btnNeg.TabIndex = 29;
            this.btnNeg.Text = "[Close Info]";
            this.btnNeg.UseVisualStyleBackColor = true;
            this.btnNeg.Click += new System.EventHandler(this.btnNeg_Click);
            // 
            // lblCancel
            // 
            this.lblCancel.AutoSize = true;
            this.lblCancel.BackColor = System.Drawing.Color.Transparent;
            this.lblCancel.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCancel.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblCancel.Location = new System.Drawing.Point(3, 0);
            this.lblCancel.Name = "lblCancel";
            this.lblCancel.Size = new System.Drawing.Size(263, 54);
            this.lblCancel.TabIndex = 21;
            this.lblCancel.Text = "Your purchase has been canceled.\r\nInformation e-mail has been sent \r\nto your addr" +
    "ess.";
            // 
            // pnlInfo
            // 
            this.pnlInfo.BackColor = System.Drawing.Color.Tan;
            this.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlInfo.Controls.Add(this.btnInfo);
            this.pnlInfo.Controls.Add(this.lblTrue);
            this.pnlInfo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlInfo.Location = new System.Drawing.Point(313, 68);
            this.pnlInfo.Name = "pnlInfo";
            this.pnlInfo.Size = new System.Drawing.Size(222, 95);
            this.pnlInfo.TabIndex = 26;
            this.pnlInfo.Visible = false;
            // 
            // btnInfo
            // 
            this.btnInfo.Cursor = System.Windows.Forms.Cursors.No;
            this.btnInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnInfo.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnInfo.Location = new System.Drawing.Point(59, 57);
            this.btnInfo.Name = "btnInfo";
            this.btnInfo.Size = new System.Drawing.Size(93, 34);
            this.btnInfo.TabIndex = 28;
            this.btnInfo.Text = "[Close Info]";
            this.btnInfo.UseVisualStyleBackColor = true;
            this.btnInfo.Click += new System.EventHandler(this.btnInfo_Click);
            // 
            // lblTrue
            // 
            this.lblTrue.AutoSize = true;
            this.lblTrue.BackColor = System.Drawing.Color.Transparent;
            this.lblTrue.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrue.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblTrue.Location = new System.Drawing.Point(3, 0);
            this.lblTrue.Name = "lblTrue";
            this.lblTrue.Size = new System.Drawing.Size(210, 54);
            this.lblTrue.TabIndex = 27;
            this.lblTrue.Text = "Thanks for your purchase. \r\nInvoice has been sent to \r\nyour e-mail address.";
            // 
            // btnUser
            // 
            this.btnUser.BackColor = System.Drawing.Color.Tan;
            this.btnUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnUser.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnUser.Location = new System.Drawing.Point(710, 84);
            this.btnUser.Name = "btnUser";
            this.btnUser.Size = new System.Drawing.Size(86, 39);
            this.btnUser.TabIndex = 27;
            this.btnUser.Text = "New User!";
            this.btnUser.UseVisualStyleBackColor = false;
            this.btnUser.Visible = false;
            this.btnUser.Click += new System.EventHandler(this.btnUser_Click);
            // 
            // btnLogut
            // 
            this.btnLogut.BackColor = System.Drawing.Color.Transparent;
            this.btnLogut.Cursor = System.Windows.Forms.Cursors.No;
            this.btnLogut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnLogut.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnLogut.Location = new System.Drawing.Point(651, 3);
            this.btnLogut.Name = "btnLogut";
            this.btnLogut.Size = new System.Drawing.Size(90, 31);
            this.btnLogut.TabIndex = 18;
            this.btnLogut.Text = "Logout";
            this.btnLogut.UseVisualStyleBackColor = false;
            this.btnLogut.Click += new System.EventHandler(this.btnLogut_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::TurnThePage.Properties.Resources.BGi;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(808, 453);
            this.Controls.Add(this.btnUser);
            this.Controls.Add(this.pnlInfo);
            this.Controls.Add(this.pnlNeg);
            this.Controls.Add(this.lblExpo);
            this.Controls.Add(this.btnPay);
            this.Controls.Add(this.pnlCart);
            this.Controls.Add(this.pnlUser);
            this.Controls.Add(this.pnlBooks);
            this.Controls.Add(this.pnlMagazine);
            this.Controls.Add(this.pnlMusicCd);
            this.Controls.Add(this.panelTop);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(808, 453);
            this.MinimumSize = new System.Drawing.Size(808, 453);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.Load += new System.EventHandler(this.FormMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbMagazine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcMusicCd)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBooks)).EndInit();
            this.pnlBooks.ResumeLayout(false);
            this.pnlBooks.PerformLayout();
            this.pnlMusicCd.ResumeLayout(false);
            this.pnlMusicCd.PerformLayout();
            this.pnlMagazine.ResumeLayout(false);
            this.pnlMagazine.PerformLayout();
            this.pnlUser.ResumeLayout(false);
            this.pnlUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbShopCart)).EndInit();
            this.pnlCart.ResumeLayout(false);
            this.pnlCart.PerformLayout();
            this.pnlNeg.ResumeLayout(false);
            this.pnlNeg.PerformLayout();
            this.pnlInfo.ResumeLayout(false);
            this.pnlInfo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnExt;
        private System.Windows.Forms.PictureBox pbMagazine;
        private System.Windows.Forms.PictureBox pcMusicCd;
        internal System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.PictureBox pbBooks;
        private System.Windows.Forms.Label lblBooks;
        private System.Windows.Forms.Panel pnlBooks;
        private System.Windows.Forms.Panel pnlMusicCd;
        private System.Windows.Forms.Label lblMusicCd;
        private System.Windows.Forms.Panel pnlMagazine;
        private System.Windows.Forms.Label lblMagazine;
        private System.Windows.Forms.Panel pnlUser;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblWelcome;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Label lblTurnThePage;
        private System.Windows.Forms.Label lblMoney;
        private System.Windows.Forms.PictureBox pbShopCart;
        private System.Windows.Forms.Label lblDolar;
        private System.Windows.Forms.Panel pnlCart;
        private System.Windows.Forms.Button btnPay;
        private System.Windows.Forms.Label lblExpo;
        private System.Windows.Forms.Timer timerMain;
        private System.Windows.Forms.Panel pnlNeg;
        private System.Windows.Forms.Label lblCancel;
        private System.Windows.Forms.Panel pnlInfo;
        private System.Windows.Forms.Label lblTrue;
        private System.Windows.Forms.Button btnNeg;
        private System.Windows.Forms.Button btnInfo;
        private System.Windows.Forms.Button btnUser;
        private System.Windows.Forms.Button btnLogut;
    }
}

