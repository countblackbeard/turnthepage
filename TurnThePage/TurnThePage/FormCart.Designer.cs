﻿namespace TurnThePage
{
    partial class FormCart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTop = new System.Windows.Forms.Panel();
            this.lblTurnThePage = new System.Windows.Forms.Label();
            this.pbLogo = new System.Windows.Forms.PictureBox();
            this.btnExt = new System.Windows.Forms.Button();
            this.dgwProducts = new System.Windows.Forms.DataGridView();
            this.gbPaymet = new System.Windows.Forms.GroupBox();
            this.rbCash = new System.Windows.Forms.RadioButton();
            this.rbCredit = new System.Windows.Forms.RadioButton();
            this.pnlCart = new System.Windows.Forms.Panel();
            this.lblDolar = new System.Windows.Forms.Label();
            this.lblMoney = new System.Windows.Forms.Label();
            this.btnBuy = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgwProducts)).BeginInit();
            this.gbPaymet.SuspendLayout();
            this.pnlCart.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.Tan;
            this.panelTop.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelTop.Controls.Add(this.lblTurnThePage);
            this.panelTop.Controls.Add(this.pbLogo);
            this.panelTop.Controls.Add(this.btnExt);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Location = new System.Drawing.Point(0, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(598, 39);
            this.panelTop.TabIndex = 9;
            this.panelTop.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelTop_MouseMove);
            // 
            // lblTurnThePage
            // 
            this.lblTurnThePage.AutoSize = true;
            this.lblTurnThePage.BackColor = System.Drawing.Color.Transparent;
            this.lblTurnThePage.Font = new System.Drawing.Font("Monotype Corsiva", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblTurnThePage.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblTurnThePage.Location = new System.Drawing.Point(60, 2);
            this.lblTurnThePage.Name = "lblTurnThePage";
            this.lblTurnThePage.Size = new System.Drawing.Size(252, 28);
            this.lblTurnThePage.TabIndex = 17;
            this.lblTurnThePage.Text = "Turn The Page BookStore";
            // 
            // pbLogo
            // 
            this.pbLogo.BackColor = System.Drawing.Color.Transparent;
            this.pbLogo.BackgroundImage = global::TurnThePage.Properties.Resources.logo;
            this.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbLogo.Location = new System.Drawing.Point(11, 3);
            this.pbLogo.Name = "pbLogo";
            this.pbLogo.Size = new System.Drawing.Size(33, 30);
            this.pbLogo.TabIndex = 16;
            this.pbLogo.TabStop = false;
            // 
            // btnExt
            // 
            this.btnExt.BackColor = System.Drawing.Color.Transparent;
            this.btnExt.Cursor = System.Windows.Forms.Cursors.No;
            this.btnExt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnExt.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnExt.Location = new System.Drawing.Point(538, 2);
            this.btnExt.Name = "btnExt";
            this.btnExt.Size = new System.Drawing.Size(48, 31);
            this.btnExt.TabIndex = 4;
            this.btnExt.Text = "X";
            this.btnExt.UseVisualStyleBackColor = false;
            this.btnExt.Click += new System.EventHandler(this.btnExt_Click);
            // 
            // dgwProducts
            // 
            this.dgwProducts.BackgroundColor = System.Drawing.Color.Linen;
            this.dgwProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgwProducts.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgwProducts.Location = new System.Drawing.Point(0, 39);
            this.dgwProducts.MultiSelect = false;
            this.dgwProducts.Name = "dgwProducts";
            this.dgwProducts.ReadOnly = true;
            this.dgwProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgwProducts.Size = new System.Drawing.Size(417, 411);
            this.dgwProducts.TabIndex = 10;
            // 
            // gbPaymet
            // 
            this.gbPaymet.BackColor = System.Drawing.Color.Tan;
            this.gbPaymet.Controls.Add(this.rbCash);
            this.gbPaymet.Controls.Add(this.rbCredit);
            this.gbPaymet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbPaymet.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPaymet.ForeColor = System.Drawing.Color.SaddleBrown;
            this.gbPaymet.Location = new System.Drawing.Point(446, 166);
            this.gbPaymet.Name = "gbPaymet";
            this.gbPaymet.Size = new System.Drawing.Size(115, 96);
            this.gbPaymet.TabIndex = 11;
            this.gbPaymet.TabStop = false;
            this.gbPaymet.Text = "Payment";
            this.gbPaymet.Enter += new System.EventHandler(this.gbPaymet_Enter);
            // 
            // rbCash
            // 
            this.rbCash.AutoSize = true;
            this.rbCash.Checked = true;
            this.rbCash.Location = new System.Drawing.Point(6, 55);
            this.rbCash.Name = "rbCash";
            this.rbCash.Size = new System.Drawing.Size(57, 22);
            this.rbCash.TabIndex = 1;
            this.rbCash.TabStop = true;
            this.rbCash.Text = "Cash";
            this.rbCash.UseVisualStyleBackColor = true;
            // 
            // rbCredit
            // 
            this.rbCredit.AutoSize = true;
            this.rbCredit.Location = new System.Drawing.Point(7, 31);
            this.rbCredit.Name = "rbCredit";
            this.rbCredit.Size = new System.Drawing.Size(100, 22);
            this.rbCredit.TabIndex = 0;
            this.rbCredit.Text = "Credit Card";
            this.rbCredit.UseVisualStyleBackColor = true;
            // 
            // pnlCart
            // 
            this.pnlCart.BackColor = System.Drawing.Color.Tan;
            this.pnlCart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCart.Controls.Add(this.lblDolar);
            this.pnlCart.Controls.Add(this.lblMoney);
            this.pnlCart.Font = new System.Drawing.Font("Modern No. 20", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlCart.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnlCart.Location = new System.Drawing.Point(446, 65);
            this.pnlCart.Name = "pnlCart";
            this.pnlCart.Size = new System.Drawing.Size(115, 33);
            this.pnlCart.TabIndex = 19;
            // 
            // lblDolar
            // 
            this.lblDolar.AutoSize = true;
            this.lblDolar.BackColor = System.Drawing.Color.Transparent;
            this.lblDolar.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDolar.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblDolar.Location = new System.Drawing.Point(84, 0);
            this.lblDolar.Name = "lblDolar";
            this.lblDolar.Size = new System.Drawing.Size(22, 21);
            this.lblDolar.TabIndex = 20;
            this.lblDolar.Text = "$";
            // 
            // lblMoney
            // 
            this.lblMoney.AutoSize = true;
            this.lblMoney.BackColor = System.Drawing.Color.Transparent;
            this.lblMoney.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMoney.ForeColor = System.Drawing.Color.SaddleBrown;
            this.lblMoney.Location = new System.Drawing.Point(12, 0);
            this.lblMoney.Name = "lblMoney";
            this.lblMoney.Size = new System.Drawing.Size(36, 21);
            this.lblMoney.TabIndex = 18;
            this.lblMoney.Text = "0,0";
            // 
            // btnBuy
            // 
            this.btnBuy.BackColor = System.Drawing.Color.Tan;
            this.btnBuy.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnBuy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuy.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuy.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnBuy.Location = new System.Drawing.Point(432, 297);
            this.btnBuy.Name = "btnBuy";
            this.btnBuy.Size = new System.Drawing.Size(72, 38);
            this.btnBuy.TabIndex = 20;
            this.btnBuy.Text = "Buy";
            this.btnBuy.UseVisualStyleBackColor = false;
            this.btnBuy.Click += new System.EventHandler(this.btnBuy_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.Tan;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.Color.SaddleBrown;
            this.btnCancel.Location = new System.Drawing.Point(510, 297);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 38);
            this.btnCancel.TabIndex = 23;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FormCart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Linen;
            this.ClientSize = new System.Drawing.Size(598, 450);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnBuy);
            this.Controls.Add(this.pnlCart);
            this.Controls.Add(this.gbPaymet);
            this.Controls.Add(this.dgwProducts);
            this.Controls.Add(this.panelTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(598, 450);
            this.MinimumSize = new System.Drawing.Size(598, 450);
            this.Name = "FormCart";
            this.Text = "FormCart";
            this.Load += new System.EventHandler(this.FormCart_Load);
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgwProducts)).EndInit();
            this.gbPaymet.ResumeLayout(false);
            this.gbPaymet.PerformLayout();
            this.pnlCart.ResumeLayout(false);
            this.pnlCart.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Label lblTurnThePage;
        private System.Windows.Forms.PictureBox pbLogo;
        private System.Windows.Forms.Button btnExt;
        private System.Windows.Forms.DataGridView dgwProducts;
        private System.Windows.Forms.GroupBox gbPaymet;
        private System.Windows.Forms.RadioButton rbCash;
        private System.Windows.Forms.RadioButton rbCredit;
        private System.Windows.Forms.Panel pnlCart;
        private System.Windows.Forms.Label lblDolar;
        private System.Windows.Forms.Label lblMoney;
        private System.Windows.Forms.Button btnBuy;
        private System.Windows.Forms.Button btnCancel;
    }
}