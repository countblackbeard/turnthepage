﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TurnThePage
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        SqlConnection connect;
        SqlCommand command;
        SqlDataAdapter sda;
        User True;
        Customer Control;
        bool log;

        List<User> getUsers()
        {
            List<User> userList = new List<User>();

            connect = new SqlConnection("Server= localhost\\sqlexpress; Database= dbTurnThePage; Integrated Security=True;");
            command = new SqlCommand();
            sda = new SqlDataAdapter();
            command.CommandText = "SELECT *FROM User_info";
            command.CommandType = CommandType.Text;
            
            connect.Open();
            command.Connection = connect;
            SqlDataReader dr = command.ExecuteReader();         
            while (dr.Read())
            {
                if ((bool)dr[6] == true)
                {
                    AdminUser temp = new AdminUser();

                    temp.CustomerID = Convert.ToInt32(dr[0]); 
                    temp.Name = dr[1].ToString();
                    temp.Adress = dr[2].ToString();
                    temp.Email = dr[3].ToString();
                    temp.Username = dr[4].ToString();
                    temp.Password = dr[5].ToString();


                    userList.Add(temp);
                }                    
                else
                {
                    Customer temp = new Customer();
                   
                    temp.CustomerID = Convert.ToInt32(dr[0]);
                    temp.Name = dr[1].ToString();
                    temp.Adress = dr[2].ToString();
                    temp.Email = dr[3].ToString();
                    temp.Username = dr[4].ToString();
                    temp.Password = dr[5].ToString();

                    userList.Add(temp);
                }
            }
            connect.Close();

            return userList;
        }

        bool userCheck()
        {
            List<User> userList = new List<User>();
            userList = getUsers();
            int i = 0;

            while (i < userList.Count)
            {
                if (userList[i].Username.Trim() == txtUsername.Text)
                {
                    if (userList[i].Password.Trim() == txtPassword.Text)
                    {
                        True = userList[i];
                        return true;
                    }
                        
                    else
                        return false;
                }
                i++;
            }
            return false;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {          
            if(userCheck())
            {
                this.Visible = false;
                using (FormMain main = new FormMain(True, Control))
                {
                    if (main.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        
                    }
                    log = main.log;
                }
                if(log==false)
                    this.Visible = true;
            }
            else
            {
                lblWrong.Visible = true;
            }
           
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void btnRegister_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            using (FormRegister fr = new FormRegister())
            {
                if (fr.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    Control = fr.custom;
                    pnlNeg.Visible = true;
                }
            }

            this.Visible = true;
        }

        private void btnExt_Click(object sender, EventArgs e)
        {
            DialogResult a = MessageBox.Show("Do you wanna close this program?", "Thank you for choosing us!", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
            if (a == DialogResult.Yes)
                this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnNeg_Click(object sender, EventArgs e)
        {
            pnlNeg.Visible = false;
        }
    }
}
