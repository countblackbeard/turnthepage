﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TurnThePage
{
    public partial class FormRegister : Form
    {
        public FormRegister()
        {
            InitializeComponent();
        }

        private void FormRegister_Load(object sender, EventArgs e)
        {

        }

        SqlConnection connect;
        SqlCommand command;
        SqlDataAdapter sda;
        public Customer custom;

        void saveUser()
        {
            custom = new Customer();

            custom.Name = txtName.Text;
            custom.Adress = txtAdress.Text;
            custom.Email = txtEmail.Text;
            custom.Username = txtUsername.Text;
            custom.Password = txtPassword.Text;
            custom.IsAdmin = false;

        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void btnExt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            
            saveUser();


        }

        private void timerRegister_Tick(object sender, EventArgs e)
        {
            if (txtPassword.Text == txtRePass.Text && cbAgreement.Checked)
            {
                btnRegister.Enabled = true;
                lblpas.Visible = false;
                lblInfo.Visible = false;
            }
            else if (txtPassword.Text != txtRePass.Text && cbAgreement.Checked)
            {
                lblpas.Visible = true;
                btnRegister.Enabled = false;
            }
            else if (txtPassword.Text == txtRePass.Text && !cbAgreement.Checked)
            {
                lblInfo.Visible = true;
                btnRegister.Enabled = false;
                lblpas.Visible = false;
            }
            else if (txtPassword.Text != txtRePass.Text && !cbAgreement.Checked)
            {
                lblpas.Visible = true;
                lblInfo.Visible = true;
                btnRegister.Enabled = false;
            }
        }
    }
}
