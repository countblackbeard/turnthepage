﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TurnThePage
{
    public partial class FormCart : Form
    {

        public ShoppingCart shoppingCart = new ShoppingCart();
        public int buy;

        public FormCart()
        {
            InitializeComponent();
        }

        public FormCart(ShoppingCart shoppingCar)
        {
            InitializeComponent();
            shoppingCart = shoppingCar;
        }

        void fillDataGrid()
        {

            dgwProducts.ColumnCount = 3;
            dgwProducts.Columns[0].Name = "Name";
            dgwProducts.Columns[1].Name = "Price";
            dgwProducts.Columns[2].Name = "Quantity";

            dgwProducts.Columns[1].Width = 50;
            dgwProducts.Columns[2].Width = 50;


            foreach (ItemToPurchase k in shoppingCart.ItemsToPurchase)
            {
                string[] row = new string[] { k.Product.Name, k.Product.Price.ToString(), k.Quantity.ToString()};
                dgwProducts.Rows.Add(row);
            }
      
        }

        private void FormCart_Load(object sender, EventArgs e)
        {
            fillDataGrid();
            lblMoney.Text = shoppingCart.PaymentAmount.ToString();

        }

        private void gbPaymet_Enter(object sender, EventArgs e)
        {

        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void btnExt_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panelTop_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnBuy_Click(object sender, EventArgs e)
        {
            shoppingCart.placeOrder();
            dgwProducts.Rows.Clear();
            dgwProducts.Refresh();
            shoppingCart.PaymentAmount = 0;           
            fillDataGrid();
            lblMoney.Text = shoppingCart.PaymentAmount.ToString();
            buy = 1;
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            shoppingCart.cancelOrder();
            dgwProducts.Rows.Clear();
            dgwProducts.Refresh();
            shoppingCart.PaymentAmount = 0;
            fillDataGrid();
            lblMoney.Text = shoppingCart.PaymentAmount.ToString();
            buy = 2;
        }

        private void timerMain_Tick(object sender, EventArgs e)
        {
            
        }

        private void btnDialog_Click(object sender, EventArgs e)
        {

        }
    }
}
