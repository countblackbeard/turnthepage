﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static TurnThePage.Magazine;

namespace TurnThePage
{
    public partial class FormMagazine : Form
    {

        public ShoppingCart shoppingCart = new ShoppingCart();
        SqlConnection connect;
        SqlCommand command;
        SqlDataAdapter sda;
        bool Admin;

        public FormMagazine()
        {
            InitializeComponent();
        }

        public FormMagazine(ShoppingCart shopping,bool admin)
        {
            InitializeComponent();
            shoppingCart = shopping;
            Admin = admin;

        }

        List<Magazine> getMagazine()
        {
            List<Magazine> magazineList = new List<Magazine>();

            connect = new SqlConnection("Server= localhost\\sqlexpress; Database= dbTurnThePage; Integrated Security=True;");
            command = new SqlCommand();
            sda = new SqlDataAdapter();
            command.CommandText = "SELECT *FROM Magazine_info";
            command.CommandType = CommandType.Text;

            connect.Open();
            command.Connection = connect;
            SqlDataReader dr = command.ExecuteReader();

            while (dr.Read())
            {
                Magazine temp = new Magazine();
                
                temp.Name = dr[0].ToString();
                temp.ID1 = Convert.ToInt32(dr[1]);
                temp.Price = (float)Convert.ToDouble(dr[2]);
                temp.Issue = Convert.ToInt32(dr[3]);
                temp.TypeOfMagazine = (Magazine_type)Convert.ToInt32(dr[4]);

                magazineList.Add(temp);

            }
            connect.Close();

            return magazineList;
        }

        void fillDataGrid()
        {

            dgwMagazine.Rows.Clear();
            dgwMagazine.Refresh();

            int i = 0;

            List<Magazine> magazineList = new List<Magazine>();
            magazineList = getMagazine();

            dgwMagazine.ColumnCount = 5;
            dgwMagazine.Columns[0].Name = "ID";
            dgwMagazine.Columns[1].Name = "Name";
            dgwMagazine.Columns[2].Name = "Price";
            dgwMagazine.Columns[3].Name = "Issue";
            dgwMagazine.Columns[4].Name = "Type";

            dgwMagazine.Columns[0].Width = 20;
            dgwMagazine.Columns[2].Width = 30;
            dgwMagazine.Columns[3].Width = 30;

            dgwMagazine.BorderStyle = BorderStyle.None;
            dgwMagazine.AlternatingRowsDefaultCellStyle.BackColor = Color.Tan;
            dgwMagazine.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dgwMagazine.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dgwMagazine.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dgwMagazine.BackgroundColor = Color.White;

            dgwMagazine.EnableHeadersVisualStyles = false;
            dgwMagazine.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dgwMagazine.ColumnHeadersDefaultCellStyle.BackColor = Color.SaddleBrown;
            dgwMagazine.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;

            while (i < magazineList.Count)
            {
                string[] row = new string[] {magazineList[i].ID1.ToString(), magazineList[i].Name, magazineList[i].Price.ToString(),
                    magazineList[i].Issue.ToString(), magazineList[i].TypeOfMagazine.ToString()};
                dgwMagazine.Rows.Add(row);
                i++;
            }
        }



        private void btnExt_Click(object sender, EventArgs e)
        {
                this.Close();
        }

        private void FormMagazine_Load(object sender, EventArgs e)
        {
            fillDataGrid();

            if (Admin == false)
            {
                this.Size = new Size(750, 501);
            }
            else
            {
                //this.Size = new Size(1085, 501);
                btnExt.Visible = false;
                btnSelect.Visible = false;
            }
        }

        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 0x84:
                    base.WndProc(ref m);
                    if ((int)m.Result == 0x1)
                        m.Result = (IntPtr)0x2;
                    return;
            }

            base.WndProc(ref m);
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void FormMagazine_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void panelTop_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            pnlInfo.Visible = false;
        }

        private void dgwMagazine_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dgwMagazine_BindingContextChanged(object sender, EventArgs e)
        {

        }

        private void dgwMagazine_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (Admin == false)
            {
                foreach (DataGridViewRow row in dgwMagazine.SelectedRows)
                {
                    Magazine temp = new Magazine();


                    //row.Cells[1].Value.ToString();
                    temp.ID1 = Convert.ToInt32(row.Cells[0].Value);
                    temp.Name = row.Cells[1].Value.ToString();
                    temp.Price = Convert.ToSingle(row.Cells[2].Value);
                    temp.Issue = Convert.ToInt32(row.Cells[3].Value);

                    int type = 0;

                    switch (row.Cells[4].Value)
                    {
                        case "Actual":
                            type = 0;
                            break;
                        case "News":
                            type = 1;
                            break;
                        case "Sport":
                            type = 2;
                            break;
                        case "Gaming":
                            type = 3;
                            break;
                        case "Technology":
                            type = 4;
                            break;
                        default:
                            type = 0;
                            break;
                    }

                    temp.TypeOfMagazine = (Magazine_type)type;

                    using (FormBuy formBuy = new FormBuy(temp, shoppingCart))
                    {
                        if (formBuy.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            shoppingCart = formBuy.shoppingCart;
                        }
                    }
                }
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dgwMagazine.SelectedRows)
            {
                Magazine temp = new Magazine();


                //row.Cells[1].Value.ToString();
                temp.ID1 = Convert.ToInt32(row.Cells[0].Value);
                temp.Name = row.Cells[1].Value.ToString();
                temp.Price = Convert.ToSingle(row.Cells[2].Value);
                temp.Issue = Convert.ToInt32(row.Cells[3].Value);

                int type = 0;               

                switch (row.Cells[4].Value)
                {
                    case "Actual":
                        type = 0;
                        break;
                    case "News":
                        type = 1;
                        break;
                    case "Sport":
                        type = 2;
                        break;
                    case "Gaming":
                        type = 3;
                        break;
                    case "Technology":
                        type = 4;
                        break;
                    default:
                        type = 0;
                        break;
                }

                temp.TypeOfMagazine = (Magazine_type)type;

                using (FormBuy formBuy = new FormBuy(temp, shoppingCart))
                {
                    if (formBuy.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        shoppingCart = formBuy.shoppingCart;
                    }
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            addBook();
            fillDataGrid();
        }

        void addBook()
        {
            int type = 0;
            connect = new SqlConnection("Server= localhost\\sqlexpress; Database= dbTurnThePage; Integrated Security=True;");
            command = new SqlCommand("INSERT into Magazine_info(name,price,issue,type) values (@name,@price,@issue,@type)", connect);
            sda = new SqlDataAdapter();



            command.Parameters.AddWithValue("@name", txtUsername.Text);
            command.Parameters.AddWithValue("@price", txtPrice.Text);
            command.Parameters.AddWithValue("@issue", txtIssue.Text);

            switch(cbType.Text)
            {
                case "Actual":
                    type = 0;
                    break;
                case "News":
                    type = 1;
                    break;
                case "Sport":
                    type = 2;
                    break;
                case "Gaming":
                    type = 3;
                    break;
                case "Technology":
                    type = 4;
                    break;                   
            }
            command.Parameters.AddWithValue("@type", type);

            connect.Open();
            //command.Connection = connect;
            command.ExecuteNonQuery();
            connect.Close();
            clearTxt();
        }

        void clearTxt()
        {
            txtIssue.Text = "";
            txtUsername.Text = "";
            txtPrice.Text = "";
            cbType.Text = "";
        }

        private void btnExt2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
