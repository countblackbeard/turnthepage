﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TurnThePage
{
    public class ShoppingCart
    {
        public enum Type_Payment
        {
            Cash,
            CreditCard
        }

        private int CustomerId;
        private ArrayList itemsToPurchase = new ArrayList();
        private float paymentAmount = 0;
        private Type_Payment paymentType;

        public int CustomerId1 { get => CustomerId; set => CustomerId = value; }
        public ArrayList ItemsToPurchase { get => itemsToPurchase; set => itemsToPurchase = value; }
        public float PaymentAmount { get => paymentAmount; set => paymentAmount = value; }
        internal Type_Payment PaymentType { get => paymentType; set => paymentType = value; }

        public void printProducts()
        {
            foreach (int i in itemsToPurchase)
            {
                Console.WriteLine(i + ",");
            }
            
        }

        public void addProduct(ItemToPurchase item)
        {
            itemsToPurchase.Add(item);
            paymentAmount += (item.Product.Price * item.Quantity);
        }

        public void removeProduct(ItemToPurchase item)
        {
            itemsToPurchase.Remove(item);
            paymentAmount -= (item.Product.Price * item.Quantity);
        }

        public void placeOrder()
        {
            sendInvoidcebyEmail(true);
        }

        public void cancelOrder()
        {
            sendInvoidcebyEmail(false);
        }

        public void sendInvoidcebyEmail(bool a)
        {
            if(a == true)
            {
                Console.WriteLine("Thank you for your purchase!");
                Console.WriteLine("Customer ID: " + CustomerId.ToString() +
                    "\n Product List: ");
                foreach (ItemToPurchase k in itemsToPurchase)
                {
                    Console.WriteLine(
                        "Product: " + k.Product.Name.Trim() + " x " + k.Quantity);
                }
            }

            if (a == false)
            {
                Console.WriteLine("Your purchase is canceled. Thank you");
                Console.WriteLine("Customer ID: " + CustomerId.ToString());
            }

            itemsToPurchase.Clear();

        }

    }
}
